---
title: Den 06 - nemožnosti
authors:
  - acluxace
date: 2013-08-03T08:23:56.000Z
tags:
  - regular
---
<p>Asi je pochopitelné, že je nemalá řádka věcí, kterých se do doby než se uzdravím budu muset vzdát. Pro mě osobně je celkem problém abstinence od prakticky všech sportů. Horolezecká stěna, kondiční posilování, kolo a dokonce ani běh nepřipadá v úvahu. Jediný vhodný pohyb v omezené míře je chůze. Ta totiž alespoň částečně dá ulevit některým svalům, které jedou za této nevšední situace přesčasy a citelně se unavují.</p>

<p>Docela vtipné je mytí, které s ortézou připomíná spíš kočku, která se snaží za každou cenu nenamočit. Dosáhnu jen na určitá místa. Žinka se najednou jeví jako geniální vynález a takové ty <a href="https://www.google.cz/search?q=pouf&amp;client=ubuntu&amp;channel=cs&amp;gws_rd=cr&amp;um=1&amp;ie=UTF-8&amp;hl=en&amp;tbm=isch&amp;source=og&amp;sa=N&amp;tab=wi&amp;authuser=0&amp;ei=fKP8UdPnGIjW7Qax44GwBg&amp;biw=1539&amp;bih=812&amp;sei=fqP8Ue-mOoXltQaS_oHADQ#um=1&amp;client=ubuntu&amp;channel=cs&amp;hl=en&amp;authuser=0&amp;tbm=isch&amp;q=shower%20pouf&amp;revid=488761542&amp;ei=0aP8UZX3IsjNtAbG5IDwCQ&amp;ved=0CA0QsyU&amp;bav=on.2,or.r_cp.r_qf.&amp;bvm=bv.50165853%2Cd.Yms%2Cpv.xjs.s.en_US.seW1cfrvSKg.O&amp;fp=74f55685538649be&amp;biw=1539&amp;bih=812">poufy</a> do sprchy jsou nepoužítelné. Nedrbou, jsou tlusté, nepojmou pořádně větší množství vody.</p>

<p>Bezva je taky <em>tyranosauří</em> slepá ulička, kdy (celkem logicky) zdravou rukou čapnete šampón a na dlaň od zaortézované ruky ukápnete požadované množství pro lesklý a hebký účes. Postavíte lahvičku šamponu, zamyslíte se a jen bezmocně koukáte na svojí <em>tyranosauří</em> pacičku, která by třeba i moc chtěla, ale prostě nemůže šampón dopravit na hlavu.</p>

<p>Zjevné problémy s návštěvou záchoda asi némá cenu komentovat. Jen uvedu, že věci teď musím dělat s rozmyslem. 30 vteřin pro přípravu na &ldquo;posezení&rdquo; je doporučené minimum.</p>

<p>Zavazování tkaniček je jedna z věcí na kterou zatím všichni nevěřícně koukali a mně se nezdá až jako takový trabl. Prostě to chce trochu šikovnosti a nahradit drhou ruku druhou nohou. Někdo to dokáže dokonce <a href="http://www.youtube.com/watch?v=si-iQpBBusM">čistě jednou rukou</a>.</p>

<p>Bezva bylo zjištění, že v kuchyňském dřezu je možné baterii vytočit až nad kuchyňskou desku. Takže mi jednak nevadí bordel v dřezu a ani cákání proudu zvysoka tekoucí vody se nekoná.</p>

<ul><li>Naopak plynový sporák bez škrtátka je hodně nepřátelské zařízení</li>
<li>Stejně jako dveře od bytu, které musím nejdřív přitáhnout a až pak můžu otáčet klíčem</li>
<li>Nebo snaha namatlat si genciánku na vnitřní stranu tváře (bez zafialovění celých úst)</li>
<li>Navlečení ponožek jednoruč</li>
<li>Spousta bezva dvoj/troj klávesových zkratek</li>
<li>&hellip;</li>
</ul><h3>Rameno</h3>

<p>K ramenu už se nechovám tak opatrně, jako když bylo &ldquo;nové&rdquo;. Nemocná ruka je pravidelně větrána, z ortézy odepnuta. Když u toho sedím, tak to není problém. Bohužel některé pohyby, které se mi (i s ortézou) občas povedou jsou dost bolestivé. Pocit jako když někdo vezme strunu od piána a začne mi jí utahovat kolem svalových snopečků a postupně je tak prořezává.</p>