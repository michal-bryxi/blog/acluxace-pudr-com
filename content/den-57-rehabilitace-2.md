---
title: Den 57 - rehabilitace 2
authors:
  - acluxace
date: 2013-10-03T15:12:44.000Z
tags:
  - regular
---
<h3>Zápěstí</h3>

<p>Jelikož se zápěstí pravidelně připomínalo, ztěžovalo mi všední používání těla a občas mi nedalo spát, rozhodl jsem se že se zeptám doktora na názor. Došel jsem si do stejné ordinace kde se mi starají o rameno. Zrentgenovali mě. Doktor černobílou fotku předloktí prohlédl a neomylně se zeptal: &ldquo;Neměl jste jako malý zlomenou ruku?&rdquo;.</p>

<p>Když jsem přitakal, vysvětlil mi že jsem měl zlomenou jen jednu ze dvou kostí, které tvoří předloktí. A ta, která musela srůstat provedla (prý celkem běžný) trik, že začala rychleji růst. Což způsobuje v ruce nepřirozené pnutí. Normálně to nemusí vadit. Jelikož ale zápěstí při pádu z kola a při používání ortézy dostalo pořádně zabrat, tak teď je takové bolavé.</p>

<h3>Rehabilitace</h3>

<p>Vířivka. Cvičení. Rehabilitační pracovnice mi řekla, že mám posunutý práh bolesti. Pod kůží ramene mám dva hřeby, které mají trochu tendenci vystupovat. Při masáži přes tohle místo to nepříjemně tahalo. Podle mě nic co by se nenechalo vydržet.</p>

<p>Celkem překvapivě jsem byl oproti první rehabilitaci hůř pohyblivý v rameni. Cvičení je potřeba opakovat alespoň obden. Je potřeba se dostávat až do bolesti a pomalu, společně s výdechem tlačit na pozici v bolesti víc a víc.</p>