---
title: Den 90 - první posilovna
authors:
  - acluxace
date: 2013-11-03T09:02:14.000Z
tags:
  - regular
---
<p>První návštěva posilovny musela nadchnout mého fitkařského parťáka. Zatímco on si dával nějakou váhu obvyklou pro zdravého dospělého chlapa, já jsem si stroje ozkoušel nejprve úplně bez cihliček. Postupně jsem se propracoval na <em>6</em> a někdy i na závratných <em>9kg</em> protiváhy.</p>

<p>Hřeby v rameni mi od chvíle, kdy mi je voperovali už slušně popolezly. Je to běžný jev, protože nejsou nijak pevně přišroubovány ke kosti. Občas to vadí. Když si na zraněné rameno lehnu. Při tření popruhu batohu o rameno. Nebo jen při zvednutí ruky, kdy se šlachy převalují přes železa. Tehdy to občas trochu zabolí. Při běžném denním používání se žádná velká bolest neděje. Onen cca půlcentimetrový hrbolek, který teď mám na začátku paže je ale dost otravný. Vizuelně, na pohmat.</p>

<p>Moje protahování v posilovně bylo to nejlepší, co jsem zatím dokázal svému porouchanému tělu dát. Systematická práce svalů po dobu 60 minut. Přesné pohyby rukou vedené předem definovanou trajektorií cvičebního stroje. Zabírání volitelnou silou - podle závaží. Různé varianty pohybu - podle typu stroje. A samozřejmě protažení ubohých svalů, které musely/musejí přebírat funkci těch atrofovaných.</p>

<p>Únavu jsem při cvičení prakticky necítil. Většina svaloviny na tom není tak bídně. Ale při určitých pohybech, zejména při vzpažení, byla cítit ostrá, bodavá bolest v rameni. I proto jsem cvičil téměř bez protiváhy.</p>