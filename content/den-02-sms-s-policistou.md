---
title: Den 02 - SMS s policistou
authors:
  - acluxace
date: 2013-07-30T10:30:00.000Z
tags:
  - conversation
---
Policista: Dobry den, Michale. Dozvedel jsem se, ze jste s tim ramenem byl na operaci. Mrzi me, ze jsem vam zavinil takove neprijemnosti, zkazil vylet na vodu a i narozeniny. Sam jsem byl kdysi srazen, tak vim, jak vykloubene rameno a vazy boli! O to vic je mi lito, ze si tim take ted prochazite!! Doufam, ze Vas v nejblizsich dnech lekari propusti do domaciho osetreni, abyste mohl byt se svymy blizkymi, kteri Vam pomuzou tyto tezke dny lepe preckat.. Kdyby to slo, radeji bych misto Vas lezel ja!! [jméno příjmení]
Já: Pitomy nahody se stavaji. Spatny je, ze si pak clovek uvedomi jak je tenka hranice mezi bezstarostnym zitim a probdelyma nocema na jipce. Nejsem na tom nijak tragicky. Chvili holt bude trvat nez se to zahoji. Diky za zajem, bylo na vas videt ze jste to necekal, ze je vam to lito. Nemam zajem si na nekom vylevat vztek. Vzhledem k probihajicimu vysetrovani mi ale neprijde rozumne byt v kontaktu. Odhaduji, ze se jeste nekdy potkame. Hezky zbytek vikendu.
Policista: To necekal.. :-( Udelal jsem chybu a samozrejme za ni prijmu trest! Hlavne aby vy jste byl co nejdriv v poradku!! Souhlasim s tim, ze bychom nemeli byt v kontaktu, jen jsem se chtel jeste jednou omluvit..