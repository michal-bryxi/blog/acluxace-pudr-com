---
title: Den 01 - operace
authors:
  - acluxace
date: 2013-07-30T10:01:47.000Z
tags:
  - regular
---
<h3>06:00</h3>

<p>Budíček, výměna kapačky, omytí vlhkou žinkou, rozhovor s anesteziologem, poslouchání kvílení staříků. Nějaká podvyživená babča si vytrhla kanylu a zakrvácela celou postel. Při pokusu o zavedení nové jehly se paní velmi nahlas vzpírá a sestřičky jí s ledovým klidem oznamují, že to je její problém. JIPka je depresivní místo.</p>

<h3>09:40</h3>

<p>Jedeme na operační sál. Co je utržené, musí se přišít. U vstupu mají hrozně vtipný přepravní pás na lidi, kterým si pacienty předávají z lůžka na lůžko. Pás má asi metr sedmdesát, takže musím krčit nohy. Jak by tímhle procpali člověka s frakturou dolní končetiny - netuším.</p>

<p>Když na sále zodpovím otázku, kterou jsem za posledních 12 hodin zodpovídal už dvacetkrát - jak se vám to stalo - rozesměju všechen přítomný lékařský personál. Sestřička mi do žíly pustí uspávadlo a v pravidelných intervalech se ptá, jestli to už působí. V momentě kdy přiznám, že se mi začíná točit hlava se snad i pousměje a s veselou průpovídkou mi dá ještě jednu dávku. Do pěti vteřin o sobě nevím.</p>

<p>Zpětně mě celkem překvapilo, že mi na operaci ortézu nechali.</p>

<h3>12:00</h3>

<p>Probouzím se na JIPce. Usínám a opět se probouzím. To vše několikrát dokola. Po několika hodinách přichází lékař a shodnem se na tom, že JIPku už nepotřebuji a převezou mě na normální, lůžkové oddělení.</p>

<p>Tady už probíhá běžný režim ledování poraněných míst. Píchání antibiotik, léků proti trombóze. Svačin, nemocničního čaje. A prvních návštěv. Přijde i lékař, který mne operoval a vysvětlí mi, že kost na místě drží dráty a vazy jsou sešité. Takže čekáme na to, až se vše zahojí a budeme moci rehabilitovat.</p>

<p>Spánek opět nic moc. Spát na boku nemůžu. Levý je úplně mimo diskuze. Na pravém pak zafixovaná ruka spadne do nepříjemné pozice. Na zádech spát neumím. Usínám v polo-leže. Bolest není nijak výrazná, ale otravuje.</p>