---
title: Den 67 - Odškodnění + policie
authors:
  - acluxace
date: 2013-10-04T23:31:18.000Z
tags:
  - regular
---
<p>Volali mi z Generální inspekce bezpečnostních sborů. Pán se slušně představil. Sdělil mi že jim přišel dopis od společnosti zastupující moje zájmy. Že si ho přečetli a v obsahu našli žádost o vydání nějakých materiálů k mému případu. No a že mi teda volá, že dopis od společnosti má tolik faktických chyb, že prostě nemůže této žádosti vyhovět.</p>

<p>Ani mě to nepřekvapuje. Podle popisu policisty mi přišlo, že prostě vyplnili nějaký mustr, který obvykle posílají a hodili ho na poštu s tím, že to třeba vyjde. Nevyšlo. Ostatní chyby by se asi daly přehlédnout, ale to že žádali o zpřístupnění informací o případu s úplně jinou <em>spisovou značkou</em>, než pod kterou jsem veden <em>já</em> bylo jaksi trochu moc.</p>

<p>Psal jsem tedy obratem do dané společnosti e-mail a bylo mi ihned odpovězeno, že není problém. Že tyhle materiály si mohou obstarat jinde. Ok, vypustil jsem to.</p>