---
title: Den 00 - nehoda
authors:
  - acluxace
date: 2013-07-29T21:35:00.000Z
tags:
  - regular
---
<h3>18:20</h3>

<p>Jedu v Plzni z <a href="http://www.klubmalychpivovaru.cz/">Klubu malých pivovarů</a> do <a href="http://www.sach-mat.cz/">Šach Matu</a> po Americké ulici. Jelikož je Wilsonův most právě v rekonstrukci využívám absence aut na silnici po které jedu. Nemusím využívat cyklostezku, po které se momentálně motá velké množství chodců. Cca 30 metrů za zastávkou MHD míjím podélně zaparkované auto policie ČR. V hlavě se stačím pochválit za to, že v KMP jsem si dal jen dva Birelly.</p>

<p>Podle pozdějšího pátrání ve vzpomínkách a rozhovorech vše začalo ve chvíli, kdy jsem předním kolem byl zhrubana úrovni zadního nárazníku policejního vozu. V ten moment posádka vozu zaregistrovala auto jedoucí do zákazu. Pravděpodobně nově vzniklého rekonstrukcí Wilsonova mostu. Stačil jsem zaregistrovat pištění gum policejního vozu způsobené rychlým startem. Pak čumák auta policie přímo v místě zamýšlené dráhy mého kola. Pak náraz. Tady <a href="http://www.endomondo.com/workouts/221310007/2838775">moje jízda</a> končí.</p>

<p>Nestihl jsem nijak zareagovat. Strhnout řízení do protisměru (naštěstí). Nebo jen dupnout na brzdy. Škoda Octavia má <a href="http://cs.wikipedia.org/wiki/%C5%A0koda_Octavia">udávanou délku cca 4,5m</a>. Podle tachometru jsem ve chvíli střetnutí jel rychlostí <em>30km/h</em>. Takže doba na zareagování před nárazem byla <strong>0.54s</strong>. Efektivní doba pro brždění ještě mnohem menší.</p>

<p>Do auta jsem narazil někde v úrovni levého, předního blatníku. Přelétl jsem přes řidítka i přes kapotu a nějak se kutálel po zemi. Detaily si nepamatuji. Jen mě překvapilo, že jsem téměř neměl odřeniny. A ačkoli jsem si myslel, že jsem narazil hlavou o zem, tak přilba nejevila známky poškození.</p>

<p>Policejní auto na místě zastavilo a z něj okamžitě vyskákali čtyři příslušníci policie ČR. Postavil jsem se na nohy a přemýšlel co se vlastně stalo. Snažil jsem se zkontrolovat, že obě nohy jsou v pořádku, zatímco mě policisté vedli k patníku, ať se posadím. Sedl jsem si a bezděčně si prosahal pravé ramoeno - v pořádku. Poté levé rameno - tohle by tu takhle nemělo být. Pak mi došlo proč to dělám. Přes adrenalin v žilách jsem cítil probouzející se bolest. Proklel jsem podivný útvar na rameni a policisté ihned začali vytáčet &ldquo;rychlou&rdquo;.</p>

<h3>18:40</h3>

<p>Policista - řidič značně vystrašený se mi ihned omlouval a začal mi vysvětlovat, že jsem musel být zrovna v mrtvém bodu, protože mě vůbec neviděl. Druhý strážník mezitím odklízel kolo, u něhož jsem si v té chvíli stačil jen všimnout atypického &ldquo;el&rdquo; tvaru předního ráfku. Třetí sehnal svědka nehody, opsal si jeho nacionále. Nevím kolikátý mi dal napít doušku studené vody. A už vůbec nevím který si vyndal z mé peněženky občanku a opsal si moje údaje a dal mi dýchnout. Mezitím jsem telefonem informoval nejbližší známé o tom co se stalo. V tu chvíli jsem už krátce, rázně oddechoval, protože se tělo rozhodlo že mi prozradí, že některé části nejsou tam, kde by měy být. Tupá, neodbytná bolest. Snažím se levou rukou nehýbat.</p>

<p>Přijela rychlá. Lehnu si na lůžko, naloží mě a začnou napichovat kanylu. Řidič, starší a evidentně zkušenější nechá práci na mladé dívce, která mi natřikrát způsobí ukrutné pálení v žilách hřbetu pravé ruky tím, jak se jí nedaří žílu napíchnout. Dají si chvíli pauzu a dívka pokračuje na mém loketním důlku pravé ruky. Tentokrát už napoprvé úspěch. Ujištění policistů, že zbytky kola někde budou a dýchnutí &ldquo;do balónku&rdquo;. Zavření dveří sanitky a nepříliš šetrná jízda s houkačkou směr Lochotínská nemocnice. Během rozhovoru s mladou medičkou se dozvím, že studuje Bc obor pro výjezdy se sanitkou a tohle je její třetí den na praxích.</p>