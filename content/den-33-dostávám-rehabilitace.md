---
title: Den 33 - dostávám rehabilitace
authors:
  - acluxace
date: 2013-09-03T21:09:33.000Z
tags:
  - regular
---
<p>Obvodní doktorka mi dnes na základě doporučení z chirurgie napsala dobropis pro rehabilitace. Je možné chodit naprosto kamkoli, kde člověka vezmou. Jednotlivá sezení si budu asi platit sám, takže je důležité abych sbíral lístečky dokazující kde všude jsem byl a kolik jsem za to zaplatil. Odhadovaná doba rehabilitace je jeden týden, ale těmhle dohadům ve zdravotnictví už nevěřím. Jsou stejně přesné jako v odvětví kde pracuju.</p>