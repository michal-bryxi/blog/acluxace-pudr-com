---
title: Den 125 - dohady s pojišťovnou
authors:
  - acluxace
date: 2013-12-11T09:02:17.000Z
tags:
  - regular
---
<p>Přibližně v pětadvacátém dnu běhu mojí žádosti o přezkoumání rozhodnutí o výši plnění pojistné události jsem zavolal na pojišťovnu a zeptal se na stav. Podle reakce operátora šlo snadno odhadnout, že ten papír zatím nikdo neviděl. Bylo mi řečeno, že na to mají <strong>30 dní</strong> a že mám případně volat poté.</p>

<p>Dvaatřicet dní od podání žádosti volám znovu a operátorka se mě ihned snaží odkázat na oněch 30 nocí, které mají na vyřízení. Projdu si s ní základní počty s kalendářem a ona se následně omluví a slíbí, že do <strong>3 dnů</strong> budu mít odpověď.</p>

<p>Pět dní poté další telefon. Opět kolečko výslýchání, vysvětlování, příslib brzké reakce. Trvám na jasném termínu. Slíbí <strong>24 hodin</strong>. Já slíbím, že se do 48 hodin ozvu, pokud se opět nebude nic dít. Operátor je zjevně trochu zaskočen, sám sebou. Slíbil termín který asi sám slibovat nechtěl. Operátor během rozhovoru zkouší i to, že ze zákona nemám nárok na jasně daný termín reakce. Ohradím se tím, že sami mi několikrát (písemně, telefonicky) potvrdili závaznost oněch 30 dní. Opět neví co odpovědět.</p>

<p>Tentokráte dostanu odpověď. Zkráceně ji lze přepsat takto:</p>

<ul><li>Součásti kola, které jsme se rozhodli neuznat do reklamace nejsou nesporně poškozeny. Doporučení o jejich nepoužívání není dostatečné.</li>
<li>Neshoda v reklamované částce je způsobena:

<ul><li>Předpokládanou menší částkou za práce na kole, díky neuznání některých součástek do reklamace.</li>
<li>Seškrtáním pláště a duše z celkové ceny opravy. Z kola byl uznán jen výplet a ráfek.</li>
</ul></li>
</ul>