---
title: Den 91 - další administrativa
authors:
  - acluxace
date: 2013-11-04T09:01:54.000Z
tags:
  - regular
---
<h4>Odpověď pojišťovně</h4>

<p>Společnost, která má zastupovat moje práva se na mou <a href="http://acluxace.tumblr.com/post/64098730538/den-81-rehabilitace-6-7-8">žádost o pomoc s pojistnou událostí kola</a> slušně řečeno vykašlala. Bez odpovědi. Bez slušného vyjádření, že toto není jejich povinnost a že se mám obrátit jinam.</p>

<p>Sesmolil jsem tedy odvolání pojišťovně sám. Bez právnických termínů, nebo hóch odkazů na paragrafy. Tak jak mi slušnost a mé vyjadřovací schopnosti dovolily:</p>

<blockquote>
  <p>Dobrý den,</p>
  
  <p>rád bych se vyjádřil / odvolal proti vašemu rozhodnutí ke věci poškození mého kola, které vedete pod číslem xxxxxxxxxx.</p>
  
  <p>Ve vaší zprávě je uváděno (cituji): &ldquo;&hellip;Z tohoto důvodu byla výše škody určena rozpočtem na opravu předního kola s výměnou ráfku a výpletu, výměnou gripů, košíku na lahve a pedálů včetně času, stráveného výměnou.&rdquo;. Vámi vyčíslená škoda je ve výši 2 687,00 Kč.
   
  Položky vyčíslené cykloservisem jsou:</p>
</blockquote>

<table><tr><th>Položka</th>
<th>Cena</th></tr><tr><td>Rámová sada 4EVER Injection včetně hl. složení</td>
<td>6 499 Kč</td></tr><tr><td>Zapletené přední kolo Deore + Mach včetně pláště Schwalbe a duše</td>
<td>1 996 Kč</td></tr><tr><td>Odpružená vidlice Suntour NRX RL</td>
<td>3 399 Kč</td></tr><tr><td>Pedály 4EVER ložiskové</td>
<td>499 Kč</td></tr><tr><td>Košík na lahev BBB</td>
<td>149 Kč</td></tr><tr><td>Představec FSA</td>
<td>490 Kč</td></tr><tr><td>Řidítka FSA</td>
<td>490 Kč</td></tr><tr><td>Gripy 4EVER červenočerné</td>
<td>299 Kč</td></tr><tr><td>Servis kola</td>
<td>1 200 Kč</td></tr></table><blockquote>
  <p>Pokud se budu vyjadřovat prozatím pouze k této části vašeho vyjádření a osekám cykloservisem doporučené položky k výměně pouze na váš seznam, pak dostanu:</p>
</blockquote>

<table><tr><th>Položka</th><th>Cena</th></tr><tr><td>Zapletené přední kolo Deore + Mach včetně pláště Schwalbe a duše</td>
<td>1 996 Kč</td></tr><tr><td>Pedály 4EVER ložiskové</td>
<td>499 Kč</td></tr><tr><td>Košík na lahev BBB</td>
<td>149 Kč</td></tr><tr><td>Gripy 4EVER červenočerné</td>
<td>299 Kč</td></tr><tr><td>Servis kola</td>
<td>1 200 Kč</td></tr><tr><td>Suma</td>
<td>4 143 Kč</td></tr></table><blockquote>
  <p>Tedy zjevně chci poukázat na nesrovnalost mezi vaší sumou a sumou cykloservisu. Jednotlivé položky mají jasně dané ceny a i když odečtu náklady na servis kola. Které samozřejmě také nárokuji, pak částka stejně nesedí. Mohl bych prosím dostat vyjádření, přesný rozpis toho jak jste se k dané částce dopracovali?</p>
  
  <p>Dále zásadně nesouhlasím s postojem [název pojišťovny] k neuznáním položek k výměně. Nesouhlasím s hezky podstrčenou větou: &ldquo;Souhlas s rozsahem poškození je potvrzen podpisem poškozeného na protokolu o prohlídce.&rdquo;, která je dle mého názoru účelově zkreslena. V zápisu likvidátora jasně stojí: &ldquo;Poškození výše nepopsaná, nebo <strong>jinak nedoložená</strong> nebudou zahrnuta do výpočtu pojistného plnění&rdquo;.</p>
  
  <p>[název pojišťovny] mne vyzvala k doložení případných dalších (skrytých) vad. A to jak písemně - dopisem. Tak ústně slovy likvidátora, který mne ujišťoval, že je naprosto v pořádku, že nezapíše rám kola (příklad) do protokolu, protože viditelně poškozen není. Nicméně rada cykloservisu je se všech srážkou namáhaných částí zbavit. Z důvodů bezpečnosti. Na kole najezdím přes 2000km ročně a riskovat, že se mi rám při vysoké rychlosti někde rozpadne se mi opravdu nechce. 
   
  Rád bych dostal odpovědi na všechny mnou položené otázky a i vznesené námitky.
   
  Děkuji.</p>
  
  <p>Ing. Michal Bryxí</p>
</blockquote>

<h4>Dodatek zápisků od GIBS</h4>

<p>Obdržel jsem telefonát od Generální inspekce bezpečnostních sborů, že se mám dostavit podepsat další sadu papírů. Šlo opět o hořko-usměvavé opomenutí kohosi ze státních zaměstnanců. Podle zákona č. 45/2013 jsem totiž nejen poškozený, ale jsem dokonce <em>oběť trestného činu</em>.</p>

<p>Byla mi přečtena moje práva, povinnosti. Odsouhlasil jsem, že si můžu zažádat o změnu identity a že můžu být, pokud budu chtít, informován až bude pachatel propuštěn z vazby na svobodu. Jak mi sám s úšklebkem na tváři potvrdil i podplukovník, který se o mne staral: Jedná se o velice důležité položky v zákoně, které se na můj případ budou jistě vztahovat.</p>