---
title: Den 11 - výslech
authors:
  - acluxace
date: 2013-08-06T12:31:00.000Z
tags:
  - regular
---
<p>&ldquo;Generální inspekce bezpečnostních sborů&rdquo; - tak takovýhle <em>hoch</em> název nesla budova v které jsem měl podat výpověď o svém případu. Za modernizovanými, plastovými, vchodovými dveřmi byla doširoka otevřená mříž a na ní nápis: &ldquo;Zákaz vstupu se zbraní&rdquo;. Pobavila mě myšlenka, že po mě někdo bude chtít <em>&ldquo;hande hoch&rdquo;</em>, aby si mě mohli prohledat. Nic takového se nedělo, přišel si pro mě podpraporčík <em>X</em>, který měl můj případ na starosti.</p>

<p>Ačkoli mě <strong>mnoho</strong> kamarádů varovalo, že případ budou chtít zamést pod kobereček, neměl jsem z toho takový pocit. Podpraporčík napsal přesně to co jsem mu napsal, realitu si nijak extra nepřikresloval. Jen občas přidal větu nebo dvě, ktreré rozumně doplňovaly moji výpověď o věci, které mě zrovna nenapadly. O řidiči auta mluvil ve třetí osobě, naprosto nezůčastněně. Nijak nemanipuloval s tím, co jsem říkal. Výpověď jsem podal bez přikrašlování, nebo přehánění. A i tak mi příšlo, že ta je vedena dost proti viníkovi nehody&hellip; Ale je to v mém zájmu, nelžu, tak proč si s tím lámat hlavu.</p>

<p>Docela mě překvapilo, že nad 5 (možná 6) týdnů léčení a je to považováno za těžké ublížení na zdraví. Celkem pech, když se zrovna tohle bude tak dlouho hojit.</p>

<p>Druhé překvapení přišlo s možnostmi narovnání. Bylo mi vysvětleno, že je v zájmu řidiče mi nabídnout smírčí cestu. Kdy si vyčíslím ušlý zisk, náklady na léčení, bolestné (?) a dohodnem se na tom, že mi tuto částku zaplatí. A já na oplátku nebudu hnát případ přes soud. Tím pádem on jakožto policista bude mít daleko jednodušší život, jelikož nebude mít nějaký záznam v jakémsi systému&hellip; Nemůžu se zbavit toho pocitu, že jde o jakousi legální formu úplatku.</p>

<p>Velké zklamání přišlo se zjištěním, že automobily Policie ČR standardně <em>nemají</em> na palubní desce kameru zachycující co se děje před vozem. Takže můj <em>pokus</em> o supermana volně přecházející v <em>pokus</em> o parakotoul bohužel nikdy neuvidím. A jelikož mám z té akce v hlavě jen dva obrázky, tak těžko říci jak to vlastně vypadalo.</p>

<p>Dozvěděl jsem se také, že případ opravdu vyšetřovaly dvě oddělení. Nejprve dopravní. Poté co doktoři prohlásili, že mi opravdu něco je, tak případ přejala generální inspekce.</p>

<p>Podepsání protokolů, teď se vám ozve pojišťovna, kolo máme my, rozloučení, domů.</p>

<h3>Rameno</h3>

<p>Konečně jsem přišel na způsob, jak se alespoň trochu prospat. Peřiny a polštáře všude kde to jde. Podepírat všude kde to tlačí. A hlavně spát v polosedě.</p>