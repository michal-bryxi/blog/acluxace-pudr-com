---
title: Den 93 - cvičení
authors:
  - acluxace
date: 2013-12-08T09:01:50.000Z
tags:
  - regular
---
<h4>Přítah na hrazdě</h4>

<p>Při návštěvě fitka jsem se už odvážil použít při tom mém protahování i nějaké to závaží. Před nehodou jsem byl v poměrně dobré fyzické kondici, takže je rozdíl toho co jsem uzvedl před třemi měsící a dnes propastný. Přesto je pozitivní, že se hmotnost kterou na strojích uzvednu návštěvu od návštěvy zvyšuje. Ze začátku jsem cvičil bez závaží spíš proto, abych si omylem neublížil než proto, že bych větší váhu neuzvedl.</p>

<p>Přesto musím přiznat, že po chvíli kvrdlání hlavice pažní kosti v kloubní jamce lopatky mě rameno a k němu náležící svaly začaly bolet. Takový pocit řezání kdesi mezi kůží a kovovým implantátem. Buď se jedná o tření svalů o výstuhy, nebo prostý fakt, že tělo danou žátěž už nezvládalo. Nedokážu určit.</p>

<p>Nejhorším &ldquo;cvikek&rdquo; bylo celkem překvapivě obyčejné pověšení se na hrazdu podhmatem. Zlobivé zápěstí mi nedovoluje vytočit pořádně ruku. Nebo alespoň ne bez bolesti. Takže jsem opět vypadal dost komicky, když jsem mocně funěl jen tak zavěsený na hrazdě a neudělal ani jeden shyb.</p>

<h4>Plavání</h4>

<p>Podstatně příjemnější bylo plavání. Obecně velmi doporučovaný sport při tomto typu úrazu. Ramenní kloub dovoluje pohyb do všech směrů, takže svaly a šlachy k němu připojené je potřeba protahovat velkým množstvím způsobů. V bazénu jsem si uvědomil, že volnost pohybu má ještě své mezery, protože do určitých poloh se musím přemáhat přes bolest.</p>