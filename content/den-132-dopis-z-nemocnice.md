---
title: Den 132 - Dopis z nemocnice
authors:
  - acluxace
date: 2013-12-12T09:02:11.000Z
tags:
  - regular
---
<p>Přišla naditá obálka z nemocnice. Manuál k nastávající operaci. Budou mi vyndávat výztuhy a dráty z ramene. Celý návod se skládá z cca šesti papírů formátu A4. Předoperační vyšetření u obvoďáka. Vyšetření anesteziologem. Dostavte se na lačno. Zajistěte si odvoz z nemocnice. Vyplňte tadytu sadu papírů&hellip; Operace je poměrně komplikovaná záležitost, pokud člověk zrovna neleží na JIPce.</p>

<p>Operace bude prováděna pod celkovou anestezií. Pokud vše půjde podle plánu, tak si poležím jen jeden den.</p>