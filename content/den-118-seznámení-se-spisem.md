---
title: Den 118 - seznámení se spisem
authors:
  - acluxace
date: 2013-12-10T09:01:00.000Z
tags:
  - regular
---
<p>Plán na dnešní den se skládal ze tří částí:</p>

<h4>1) Náhlednutí do spisu</h4>

<p>Dnes policista uzavíral můj spis a postupoval ho dále v hierarchii vyšetřování. Měl jsem možnost kdykoli do dnešního dn spis přijít prostudovat, udělat si výpisky, či pořídit (na vlastní náklady) kopie. Šanon měl něco přes <strong>120 stran</strong> a cédéčko s fotkami nehody. Jako zajímavé mi přišlo to, že se muselo zaznamenat které všechny informace jsem si zkopíroval. Prý proto, až se někde tyto informace objeví, aby se vědělo kdo je vynesl ven.</p>

<p>Listuju stránkami. Už asi podesáté čtu v drobných obměnách popsanou tu samou historku - příběh kolize mého kola a policejního auta. Ačkoli jsou příbehy popsané podobně, lze vidět určité citové zabarvení, podle toho která strana vypovídala.</p>

<p>Jako laikovi se základními znalostmi <em>technické dokumentace</em> ze střední školy mi přišlo, že náčrtek nehody (pořízený pravděpodobně dopravní policií) nebyl technicky úplně správně proveden. Po troše luštění jsem odhadl, že délka letu cyklisty (mě) od střetu s automobilem až po finální dokutálení se byla <strong>4,1m</strong>. Jednoznačně nejdelší skok do dálky, který jsem kdy udělal.</p>

![Náčrtek nehory](/images/den-118-seznameni-se-spisem.jpg)

<h4>2) Setkání s právníkem podezřelého</h4>

<p>Policista, který řídil auto které mě srazilo byl od začátku z celé situace dost nešťastný. Omlouval se mi ihned na místě, později SMSkou a dnes i ústy právníka. Nabídli mi finanční kompenzaci za způsobené problémy a příslib případné náhrady vzniklých škod, které bych nebyl schopen vyřídit s pojišťovnou.</p>

<h4>3) Stanovení ušlého zisku</h4>

<p>Po nějakých dohadech s člověkem, který má na starosti vyšetřování mého případu jsme se shodli na tom, že nejlepší odhad částky kterou mohu po pojišťovně viníka žádat jako kompenzaci ušlého zisku je ekvivalent peněz z  mého úrazového pojištění. Jelikož jsem ještě peníze z úrazovky nenárokoval, dal jsem do spisu zanést pouze odhad na základě tabulkových výpočtů.</p>
