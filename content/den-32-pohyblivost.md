---
title: Den 32 - pohyblivost
authors:
  - acluxace
date: 2013-08-26T19:11:51.000Z
tags:
  - regular
---
<p>Dnes ráno to jsou tři dny co mi sundali ortézu a já jsem svojí levačku začal opět zapojovat do společenského života. Už se jen tak neválí, už pěkně maká. S výkonem tak tříletého dítěte. Nic moc, ale je to začátek. V ruce zatím rozumně neudržím ani hrnek s čajem.</p>

<p>Pravidelně cvičím, zatím pořád s dopomocí. Úhel do kterého dokážu ruku zvednout se nijak nezvětšil. Ale aspoň to jde s menší námahou. &lt;vtip&gt; Holt pozdrav vůdci ještě nějakou chvíli dělat nebudu. &lt;/vtip&gt;</p>

<p>Ruka ale v zápěstí a prstech získává citelně na síle a například navlečení ponožek už téměř zvládám obouruč. Nejhorší jsou úkony při kterých musím ruku dostat od těla. Ty zatím bolí. Například sezení v kombinaci vysoký stůl + nízká židle je pro práci na počítači naprosto nevhodné. Ruce jsou pak nuceny &ldquo;dopředu&rdquo;, což zatím moc nefunguje.</p>

<p>&lt;reklama&gt; Na poničené zápěstí jsem od doktorky dostal mast - <a href="http://www.docsimon.com/article/almiral-gel-100g">Almiral</a>. Už několikrát jsem si vyzkoušel, že umí krásně utlumit bolest v zápěstí. Doporučuji. &lt;/reklama&gt;</p>