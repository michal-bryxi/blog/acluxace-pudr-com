---
title: Den 43 - právní rozcvička
authors:
  - acluxace
date: 2013-09-10T15:53:14.000Z
tags:
  - regular
---
<p>Z předchozího dne (42) jsem byl telefonicky dohodnut s policistou, který se mnou sepisoval <a href="http://acluxace.tumblr.com/post/57512720625/den-11-vyslech">protokol o nehodě</a>, že se dnes můžu stavit a vyzvednout si kolo &hellip; Nebo spíš jeho zbytky.</p>

<p>S podpraporčíkem jsme se potkali před budovou, zapovídali se a došli k vrátnici. Můj doprovod - ostřílený policista v civilu televizně vytáhl &ldquo;placku&rdquo; z bundy a nechal si od vrátného zavolat pana <strong>A</strong>. Vrátný se chvíli prohraboval seznamem zaměstnanců a vytočil číslo pana <em>A</em>. &ldquo;Nikdo to nebere&rdquo;, prohlásil asi po minutě naslouchání tůtajícího telefonu. &ldquo;Dobře, tak mi dejte pana <strong>B</strong>&rdquo;, odvětil můj doprovod. Opět stejný vysledek. V tomto režimu jsme se dostali až k panu <strong>E</strong>, který byl naštěstí přítomen.</p>

<p>Na místě jsme podepsali dodatek k mé předchozí výpovědi, který zohledňoval nějaké letošní změny v zákonech. Nic důležitého. Zajímalo by mě ale co by se stalo, kdybych odmítl. Je výpověď neplatná? Dopustil jsem se nějakého přestupku? Je to moje hloupost, nebo chyba policisty? Zákony jsou vtipná věc.</p>

<p>Podepsali jsme také předávací protokol pro kolo, &ldquo;neboť tohoto jízdního kola není již potřeba pro trestní řízení, vedené pod shoda uvedenou sp.zn.&rdquo;. Z toho jsem neměl moc dobrý pocit. Nikdo z pojišťovny kolo neviděl a pokud bych ho cestou ještě několikrát hodil ze skály, tak mi tuhle &ldquo;zlotřilost&rdquo; nemají jak prokázat.</p>

<ul><li>Policista, který se o mě doteď staral mi dal telefonní číslo na <em>policejního právníka</em> pro dořešení mých záležitostí s Policií ČR. Rozloučili jsme se.</li>
<li>Zavolal jsem právníkovi. Ten mi dal kontakt na <em>Automobilové oddělení policie ČR</em>.</li>
<li>Toto oddělení mne odkázalo na společnost u které je PČR pojištěna.</li>
<li>Na pojišťovně jsem nahlásil <em>pojistnou událost</em>. A odkázali mě na <em>likvidátora</em>, který přijede kolo nafotit.</li>
<li>Likvidátor si se mnou domluví schůzku v následujících dnech.</li>
</ul><h4>Pojistná událost</h4>

<p>Paní na pojišťovně byla taková ta typická nakrátko střižená, nagelovaná babča se spoustou elánu a kůží jako ze solárka. Za ní seděl u počítače mladý a znuděný klučina, který se za celou dobu mé návštěvy ani nesnažil předstírat že na něčem pracuje.</p>

<p>Chvíli jsme hledali můj případ v systému pojišťovny. Velmi důležité bylo, že jsem si od <em>právníka policie ČR</em> vyžádal <strong>číslo pojistné události</strong>. Zajímavé bylo, že mi <em>sám od sebe</em> předal jen informace o pojistné události kola. Nikoli už o zranění osoby. Tu jsem si musel <strong>vyžádat extra</strong> větou: &ldquo;Řeší se v daném případu ještě jiné pojistné události, než ta s kolem?&rdquo;.</p>

<p>Paní za přepážkou byla léty ostřílená, nebojácná pracovnice, která se nebála před klientem (mnou) sem tam utrousit vtipnou poznámku na účet IT oddělení své organizace. Na dotaz počítače, kde bylo moje auto v době nehody hbitě odpověděla, že <em>zaparkované</em> a přes brejličky mi oznámila, že s nehodou čehokoli jiného než dvou aut ten systém prostě neumí pracovat.</p>

<p>Dostal jsem na později papír pro <em>Potvrzení o ztrátě na výdělku</em> a <em>Posudek o bolestném</em>. Obojí se bude řešit později.</p>

<p>Důležité zjištění je také to, že ačkoli obě pojistné události Policie ČR řádně nahlásila. A pojišťovna měla veškeré potřebné podklady (sám jsem jim dodal jen doklad o koupi kola). Sama pojišťovna nezačne <strong>nic</strong> dělat, dokud pojistnou událost nenahlásí i druhá strana. Paní to zpoza monitoru s úsměvem komentovala: &ldquo;No, dokud si to nenahlásíte i vy, tak my jsme v klidu. Máme leháro, nohy na stole&hellip;&rdquo;</p>