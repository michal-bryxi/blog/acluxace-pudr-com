---
title: Den 02 - klid
authors:
  - acluxace
date: 2013-07-30T10:23:09.000Z
tags:
  - regular
---
<p>Nekonečné hodiny sezení, ležení, procházení se. Každá poloha po nějaké době začne bolet, nebo tlačit. Pravidelně přikládám led. Lehká, vtíravá bolest nechce odeznít. Při špatném pohybu to vždy v rameni štípne. Návštěvy dělí pomalu ubíhající den na kratší, snesitelnější intervaly. S rukou nemůžu díky ortéze hýbat. Začíná mě bolet zápěstí na zafixované ruce. Nemá žádnou oporu, musí občas něco podržet a není ve vůbec přirozené pozici. Na spánek na zádech jsem si už zvykl, ale stejně jsem alespoň pětkrát za noc vzhůru.</p>