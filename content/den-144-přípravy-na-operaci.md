---
title: Den 144 - Přípravy na operaci
authors:
  - acluxace
date: 2013-12-20T23:22:46.000Z
tags:
  - regular
---
<h4>Pondělí</h4>

<p>Pondělí ráno. Vesele připochoduju do ordinace mé obvodní doktorky s papíry z nemocnice. A že bych je potřeboval vyplnit. Doktorka se zeptá kdy nastupuju. Povím jí, že ve čtvrtek. A tady moje veselost na tři dny skončí.</p>

<p>Dostanu kázání, že takhle rychle se to nedá stíhat. Že tři dny na předoperační vyšetření <em>může být</em> málo. Jakožto laik jen nechápavě koukám. Na průvodních papírech pro příjem do nemocnice byl jediný pokyn: &ldquo;Vyšetření nesmí být v době operace starší 10 dnů&rdquo;.</p>

<p>Test sluchu, test zraku, tlak, teplota, EKG, několik testů z rozboru krve, rozbor moči, nemoci a případné smrti dvou předchozích generací mé rodiny, soupis mých vlastních operací a nemocí. To je zjednodušený seznam vyšetření, které jsem musel podstoupit.</p>

<p>Jelikož jsem takový kolotoč nečekal, nepřišel jsem ten den na lačno. Odběry krve se tedy musely odložit na zítra. EKG mi pravděpodobně z důvodu špatného přisátí špuntíků na hrudník vyšlo (laicky řečeno) prapodivně, takže pokračování na odborném pracovišti opět den následující. U doktorky jsem strávil přibližně <em>3 hodiny</em>.</p>

<h4>Úterý</h4>

<p>Ráno na lačno. Přinést moč. Odebrat krev. Pro urychlení vechny vzorky osobně odnést na rozbor do nedaleké laboratoře. A hurá na specializované pracoviště, kde mi &ldquo;natočí&rdquo; EKG.</p>

<ul><li>08:30 - oficiální otevírací doba pracoviště</li>
<li>09:00 - přichází doktorka</li>
<li>09:30 - odchází první pacient a já vyposlechnu následující rozhovor:

<ul><li>pacient: Sestři, já bych tady potřeboval tohle vyšetření.</li>
<li>sestra: A jste objednanej?</li>
<li>pacient: Ne.</li>
<li>sestra: No, tak to nejdřív za tři měsíce.</li>
</ul></li>
</ul><p>Poté přistupuji já a dávám sestře papíry od obvoďačky. Prolítne očima požadavek a zeptá se do kdy to potřebuju. Po mé odpovědi, že &ldquo;zítra&rdquo; jen obrátí oči v sloup, pokrčí rameny a řekne ať chvíli počkám. Po dalších 30 minutách jsem pozván do ordinace, sestřička na mě připojí měřidla. A z tiskárny už leze papír s onou známou klikatou čárou. Další čtvrthodina čekání přede dveřmi a dostávám potvrzení od doktorky, že jsem v pořádku.</p>

<h4>Středa</h4>

<p>Ráno vyzvednout výsledky rozborů, donést veškeré papíry k doktorce, získat razítko. Jsem způsobilý k operaci. Cesta do nemocnice kde proběhne operace. Vyšetření velmi milou doktorkou, která mi vše velmi ochotně, do detailů a technicky vysvětlila, protože &ldquo;doma má taky inženýra&rdquo;. Cesta na dětské oddělení chirurgie. Tam čekám mezi haldou maminek s nahými kojenci. Připadám si divně. Při první příležitosti vecpu sestřičce lístek, ona vše zkontroluje, potvrdí mou účast na zítřejsí operaci a jdu domů.</p>

<p>Po třech dnech jsem <em>připraven</em> na lékařský zákrok. Jestli by nebylo snažší nechat se někde zase srazit autem&hellip;</p>