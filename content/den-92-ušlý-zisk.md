---
title: Den 92 - ušlý zisk
authors:
  - acluxace
date: 2013-12-06T09:02:26.000Z
tags:
  - regular
---
<p>Od zaměstnance *Generální inspekce bezpečnostních sborů&quot;, který má můj případ na starosti jsem dostal domácí úkol: Spočítat si _ušlý zisk_. Pochopil jsem to tak, že mám dát dohromady všechny náklady, které mi společně s mou malou policejní patálií vznikly. Tuto částku poté budu moci požadovat jako kompenzaci od pojišťovny.</p>

<p>Evidentní náklady se prokazují snadno. Například: platby za pobyt v nemocnici, regulační poplatky u lékařů, lístky na MHD pro dopravu do nemocnice, zničené oblečení, ušlý zisk na platu (ponížený o nemocenskou), &hellip;</p>

<p>Jak ale naložit s časem, který jsem v dané kauze ztratil? Čas strávený papírováním. Doba na rehabilitacích. Propálené hodiny v učení se běžným denním postupům &ldquo;jednoruč&rdquo;. Tohle všechno je čas, který by chtěl přenásobit mojí hodinovou mzdou a výslednou částku připsat do položky <em>ušlý zisk</em>.</p>

<p>Jak se ale ukáže - můj čas není pro pojišťovnu žádná počitatelná veličina. Potřebují faktury, účtenky. Snad abych si najal Inda, aby mi můj případ spravoval a mohl mi tuto činnost vyfakturovat.</p>