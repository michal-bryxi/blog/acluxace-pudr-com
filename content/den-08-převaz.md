---
title: Den 08 - převaz
authors:
  - acluxace
date: 2013-08-03T10:25:00.000Z
tags:
  - regular
---
<p>Jedeme na převaz. Osm hodin ráno a u fakultní nemocnice Lochotín se nedá zaparkovat.</p>

<p>Popis o fungování regulačních poplatků je v čekárně žádný, nijaký. Jelikož nesleduji zprávy a nečtu noviny, tak jen matně tuším, že asi budu potřebovat regulační známku. Tušení bylo správné, později mi jí zabavili.</p>

<p>Čekací doba od předání pozvánky sestře po pozvání do ordinace - <strong>145 minut</strong>, venku jsem byl za <em>5</em>. To jsem vážně nečekal. Když jsem jen kouskem věty naznačil, že  bych se pro příště rád objednal na konkrétní hodinu, přišlo od doktora jen rázné: &ldquo;Ne, to neumíme&rdquo;. Uvidíme se za týden. Raději si s sebou vemu knížku.</p>

<p>Prvně mi sundali ortézu úplně. Ten pravděpodobný zápach jsem jim nezáviděl. Pravděpodobný proto, že sám sebe necítím. Na rameni mám sešití v délce poloviny klíční kosti. Takže operace zdá se nešla dělat nějakým malým vpichem. Sestra mi důrazně vysvětluje, že ránu při sprchování nesmím namočit. Doktor stroze sděluje, že odebrání ortézy bude možné teprve za <strong>5 až 6 týdnů</strong>, tedy mezi <strong>dnem 43</strong> a <strong>dnem 50</strong>. To jsem taky nečekal.</p>

<p>Poprvé si beru oblečení pod ortézu. Konečně nepobíhám po světě s holým pupkem, Košili asi velmi brzy propotím, ale je to 100x příjemnější, než ortéza na holé tělo.</p>

<p>Doteď jsem bydlel u přítelkyně a její rodiny, kde o mě bylo <a href="http://cs.wikipedia.org/wiki/Dna">královsky</a> postaráno. Nyní se přesouvám do svého bytu. Naštěstí mě přítelkyně umluvila, abysme cestou udělali obrovský nákup. Zpětně mi došlo kolik práce mi to ušetřilo. Tahat jakékoli břemeno je prostě problém.</p>

<h3>Policie</h3>

<p>Dovolal se mi policista z vyšetřovací skupiny, kterou jsem si poznačil jako <em>skupina #2</em>. Domluvili jsme se, že v <em>den 11</em> se dostavím do centra na výslech.</p>