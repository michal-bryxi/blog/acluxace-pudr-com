---
title: Den 365 - Rok poté
authors:
  - acluxace
date: 2014-07-26T22:09:22.000Z
tags:
  - regular
image: /images/den-365-rok-pote.jpg
---
<p>Jeden rok se zdá jako neuvěřitelně dlouhá doba. Doba, za kterou stihnete dodělat velkou spoustu věcí. Některé se mi opravdu podařilo uzavřít, jiné stále na své rozhřešení čekají.</p>

<h4>Rameno a zápěstí</h4>

<p>Velmi pozitivní na celém tom příběhu je to, jak si lékaři dokázali dobře poradit s tou lidskou skládačkou, kterou přivezla rychlá od místa nehody. Celkem rutinní operace na sešití, později následovala čtyřiadvacetihodinová chirurgická rozcvička na vyndání želez, poté nějaké to rehabko a lego panáček je skoro jako nový.</p>

<ul><li>Asi <strong>17</strong> dní od nehody jsem začal mít značné problémy s lenivěním svalů na zápěstí zraněné ruky.</li>
<li><strong>30</strong> dní od nehody mi sundali ortézu a já mohl zraněnou ruku opět začít používat.</li>
<li>Den <strong>41</strong> a já stále nedám ruku víc jak 40º od těla.</li>
<li><strong>64</strong> otočení země kolem vlastní osy a já zvládám vlastními svaly dostat ruku od těla na 90°.</li>
<li>Den <strong>75</strong> a dokážu už zmáčknout pěnový balónek. Ruka sice rozklepaně, ale dostane se až nad hlavu.</li>
<li>Ve dnu <strong>90</strong> probíhá první testovací návštěva posilovny. Účast spíše symbolická.</li>
<li>Trvalo přibližně <strong>100</strong> dní, než jsem se dostal do stavu kdy mě následky nehody nijak <em>významně</em> neomezovaly.</li>
<li>Po asi <strong>160</strong> dnech jsem se mohl přestat omezovat. Jak v běžném životě, tak při sportu.</li>
</ul><p>Velký dík patří rehabilitační pracovnicí, která mě bez milosti lámala, protahovala zkrácené svaly a cvičila se mnou ty atrofované.</p>

<p>Kvantitativně největší přínos na zotavení vidím v mém aktivním, sportovním životě. První pokusy o kliky, přítahy na hrazdě, běh, kolo, koloběžka, plavání, fitko&hellip; Odpočinek kombinovaný s aktivním gaučingem rekonvalescenci moc nepřidá.</p>

<p>Kvalitativně je to pak plichta mezi lezením na umělé horolezecké stěně a výše zmíněnýma rehabilitacema. Lezení nutí člověka vydávat hodně velkou sílu v určitých momentech, určitou skupinou svalů. Těžko bych hledal jiný sport, který bych na <em>AC luxaci</em> spíš doporučil.</p>

<h4>Trvalé následky</h4>

<p>Pojišťovna bude po roce zjišťovat trvalé následky. Zápěstí je až na občasné křupnutí v pořádku. V ramenu ale přeci jen zůstal jeden nešvar. Při větší námaze při pohybu &ldquo;otevírání posuvných dveří&rdquo; levou rukou v sešitém úponu něco nepěkně píchne.</p>

<p>Jiné problémy nemám a na silnici se vjet s kolem nebojím :)</p>

<h4>Situace řidiče</h4>

<p>Ten, který řídil ono nešťastné vozidlo mi k mému velkému překvapení asi ve dnu <strong>300</strong> volal, že se mu konečně ozval šetřící orgán a že se bude něco dít. Tedy bezmála rok trvalo, než se taková banalita na jejímž průběhu se <em>víceméně</em> shodly všechny strany vůbec dostala na jednací stůl. Datum vyřešení zatím neznámé.</p>

<h4>Finanční kalkulace</h4>

<p>Peníze, které jsem díky tomuto případu získal jsou z pěti zdrojů:</p>

<ul><li>Od společnosti, která hájila moje zájmy a šla po krku pojišťovně policie ČR</li>
<li>Moje vlastní úrazové pojištění</li>
<li>Nemocenská</li>
<li>Platba za opravu kola od pojišťovny policie ČR</li>
<li>Kompenzace od řidiče vozidla</li>
</ul><p>Celkové přímy byly <em>82 597 Kč</em>.</p>

<p>Na druhou stranu rovnice musím dát:</p>

<ul><li>Ušlý plat</li>
<li>Čas, který jsem strávil v nemocnici</li>
<li>Dobu strávenou na rehabilitacích</li>
<li>Hodiny strávené nad papírováním a vyřizováním</li>
</ul><p>Při <em>hodně</em> zjednodušeném propočtu jsem došel k závěru, že momentálně jsem na nule. Což není špatné. Nicméně sám vím, že mám tendenci věci zlehčovat, takže budu pravděpodobně mírně v mínusu.</p>

<h4>Závěr</h4>

<ul><li>Úrazové pojištění je překvapivě užitečná věc.</li>
<li>Cvičit, cvičit, cvičit. Na rehabilitacích, po ránu doma, při pravidelném sportu a podle možností i ve fitku. Samo se to nespraví.</li>
<li>Pokud máte čerstvě sešité rameno, nekýchejte!</li>
<li>Jak je to jen trochu možné, procvičujte prsty zraněné ruky. Atroface svalů je neuvěřitelně rychlá.</li>
</ul>
