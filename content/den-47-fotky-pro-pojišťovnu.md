---
title: Den 47 - fotky pro pojišťovnu
authors:
  - acluxace
date: 2013-09-10T16:34:00.000Z
tags:
  - regular
---
<p>Telefonát od pána z pojišťovny, že chce nafotit kolo. Do hodiny přijel, sepsal vše jak jsem mu nadiktoval - odřeno tady, ohnuto támhle, tohle tu taky předtím nebylo. Mám si dojít s kolem do servisu a nechat udělat odhad oprav včetně mnou prosazovaného ultrazvuku rámu. Poté pojišťovna rozhodne jak se k případu postaví. To zní fér.</p>

![Kolo zepředu](/images/den-47-fotky-pro-pojistovnu-kolo.jpg)
![Jak od Salvadora Daliho](/images/den-47-fotky-pro-pojistovnu-osma.jpg)

<h4>Rameno, zdraví, ostatní</h4>

<p>Co se týče zdraví, můžu s radostí nahlásit že se dějí znatelné pokroky.</p>

<ul><li>Spánek už asi tak týden není problém. Spím nepřerušovaně celou noc. Jen si musím dávat pozor na zápěstí, protože jeho zdraví zdá se silně závisí na poloze ruky v noci. Zápěstí musí být vždy ve vzpřímené poloze, jinak ráno bolí.</li>
<li>V noci na dnešek se mi podařilo chvíli ležet na zraněném rameni. WooHoo, teď už zvládnu cokoli! Spánek na nemocné straně by teoreticky byl možný, ale železo které mám v rameni má snahu tlačit zevnitř na kůži. Je z toho pocit jakoby Wolverinovy drápy chtěly vylézt ven &hellip; z ramene.</li>
<li>Podařilo se mi kleknout si na zem, sednout na nohy. Položit dlaně na zem, natáhnout ruce daleko před sebe. Až jsem se dotknul čelem země.</li>
</ul>
