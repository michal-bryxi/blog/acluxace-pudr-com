---
title: Den 09 - doma
authors:
  - acluxace
date: 2013-08-04T07:47:09.000Z
tags:
  - regular
---
<p>Sám doma. Jídlo tu je. Stejně tak všudypřítomný nepořádek. Uklízení, byť průběžné je překvapivě složitější jen s jednou rukou a omezenou pohyblivostí. Přenášení více věcí není prováděno s poloviční efektivitou, jak by se vzhledem k handicapu mohlo jevit, ale odhadem jsem výkonem tak na 30%.</p>

<p>Provedli jsme pokus se sundáním ortézy. Svaly zdá se rameno na místě udrží. Jen nesmím ani při riziku pádu přímo na čumák dát nemocnou ruku od těla. Přetrhání vazů téměř jisté. Následovala téměř celotělová sprcha (hurá!), vysušení ortézy, nové tričko a <em>hurá</em> zpátky do mučícího nástroje.</p>

<p>Myslel jsem si, že spánek už mám vychytaný, ale po dvou hodinách koukání do stropu jsem se přesunul do obýváku, kde jsem vytuhl v pozici polosedícího opilce a podřimoval až do rána. Spolubydlící vstává poměrně brzy, takže ani ne po čtyřech hodinách spánku mě probudilo chrastění nádobí. Pokus o pohyb. Nepříjemná ztuhlost krční páteře. Už chápu, proč je takový trest &ldquo;spát na gauči&rdquo;. Trochu se rozpohybuju a jdu testovat světlotěsnost očních víček zase zpátky do postele. Celkově jsem odhadem naspal 5 hodin.</p>

<p>Pro spánek s ortézou je potřeba kopa různě velikých polštářků, nebo alespoň improvizace v podobě přeložených ručníků. Jinak se nedá pořádně spát. Loket s ortézou je třeba pořádně podložit, zafixovat.</p>