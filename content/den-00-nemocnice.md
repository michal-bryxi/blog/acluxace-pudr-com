---
title: Den 00 - nemocnice
authors:
  - acluxace
date: 2013-07-30T09:13:00.000Z
tags:
  - regular
---
<h3>19:15</h3>

<p>Za klasického, hlučného doprovodu modrých sirén jsme dojeli do <a href="https://www.google.cz/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;ved=0CC8QFjAA&amp;url=http%3A%2F%2Fwww.fnplzen.cz%2F&amp;ei=yUf3UdCZI4XftAbKw4H4Cw&amp;usg=AFQjCNGaIvUdG23R1R13zfQI1VipQK2dNg&amp;sig2=xlqHpr9fHfWY5nbJ6tArkA&amp;bvm=bv.49967636,d.Yms">Fakultní nemocnice na Lochotíně</a>. Vyložení, předání na příjem, rozloučení s posádkou sanitky, přivítání se sestřičkami z příjmu. Jedna z nich na mě zběžně koukne, prošahá rameno a ihned prohlásí: Jo, to bude <em>[přesný lékařský termín nebyl zapamatován]</em>. Celkem mě překvapuje jak může takhle rychle odhadnout diagnózu.</p>

<p>Probíhá klasické kolečkno dotazování na průběh nehody. Zda jsem se uhodil hlavou o zem. Jestli je mi na zvracení. Na co mám alergii. A tak podobně. Poté mi sestry chtějí sundat cyklistický dres:</p>

<ul><li><strong>Sestra:</strong> Jdem sundávat tričko.</li>
<li><strong>Já:</strong> Tenhle uplej, cyklistickej dres přes tohle rameno?</li>
<li><strong>Sestra:</strong> No, pak ho můžem ještě rozstříhat&hellip;</li>
<li><strong>Já</strong>: Ten je tejden novej. To ne. To to zkusíme.</li>
</ul><p>Nakonec to nebyl žádný problém. Trik spočíval ve vyrvání pravé (zdravé) ruky z oblečení. Poté už to šlo samo.</p>

<p>Nevím už přesně, jestli to nebylo mezi některými dvěma vyšetřeními, ale v jednu chvíli se vedle mého jezdícího lůžka objevili policisté. Jiné oddělení, aby mohli vyšetřit takovýhle vnitřní problém. Dostávám opět dýchnout a hromadu otázek ohledně průběhu nehody. Prozkoumáním přilby dojdem k závěru, že jsem se asi do hlavy při pádu neuhodil - přilba je netknutá. Dostanu kontakt na praporčíka Windenšla a příslib, že se mi časem ozvou. Přesedám do pojízdného křesla. Jedeme na rentgen.</p>

<h3>19:57</h3>

<p>Obsluha rentgenu se nejprve přesvědčí, že zvládnu stát. Poté mě postaví mezi mohutná křídla generátoru <em>paprsků X</em> a pak jako vždy zmizí za dostatečně tlustými (olověnými?) dveřmi. První snímek. Mám se otočit. Lehce se mi zvedá žaludek. Začnem fotit druhý snímek. Přidá se neurčitý pocit motání hlavy, hučení v uších, stmívání. Nic moc pocit nahlásím obsluze. Zpátky dostanu jen nechtěné povzbuzení: &ldquo;Ještě chvilku vydržíme a bude druhá fotečka&rdquo;. Vydržel, ale pak se skoro sesypal do nedalekého křesla. Hukot se zvyšuje, obraz černá, studený pot, rychlý tlukot srdce. Tělo naznalo, že už je čas a upadá do něčeho co bych jako laik nazval <em>úrazový šok</em>. Odborně šlo o: <em>nauzea a vertigo se spontánní úpravou</em>. Zbytek retgenu doděláme vleže.</p>

<h3>20:17</h3>

<p>Sonografické vyšetření neodhalí žádné vnitřní poranění.</p>

<h3>20:35</h3>

<p>Přijímací zprávu se mnou sepisuje doktor, který jako kdyby z oka vypadl <a href="http://cs.wikipedia.org/wiki/Pavel_Li%C5%A1ka">Pavlu Liškovi</a>. Stejná zmatená mluva. Stejný nepřítomný výraz. Při pohledu do jeho očí jsem si představoval ty desítky vnitřních konverzací, které se mu musí v hlavě odehrávat zatímco formuluje jednu prostou otázku, kterou mi vzápětí položí. Podle sestřiček je to nějaký <em>ten génius</em>. Ale nadšené ze spolupráce s ním nebyly.</p>

<p>Diagnóza je po všech těch odběrech a vyšetřeních jasná. Jsem přiměřeně zdravý jedinec momentálně trpící <strong><a href="http://www.riversideonline.com/source/images/image_popup/ans7_shoulderseparation.jpg">AC luxací - tossy III</a></strong>. Podle toho co jsem vyslechl od doktorů a našel si na internetu jde o: přetřžení třech šlach spojujících klíční kost a lopatku.</p>

<h3>21:30</h3>

<p>Sestřičky mi nasadily ortézu a následoval převoz na JIPku. Kde mi připojily všemožná sledovátka a měřidla. Rozpíchaly dosud nepoužité žíly a nechaly mě prospat se.</p>

<p>Chvíli po převozu přišli nějací dva strážníci (neuniformovaní, pouze se prokázali &ldquo;plackami&rdquo;) a zeptali se na pár drobností. Že by na případ nasadili dva týmy? Vždyť už jakouž takouž předběžnou zprávu se mnou někdo sepisoval?</p>

<p>Odhadem jsem zavřel oči na dvě hodiny. Naříkání staříků okolo a tupá bolest v rameni nebyly moc příhodnými podmínkami pro spánek.</p>