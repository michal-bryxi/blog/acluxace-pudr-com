---
title: Den 64 - rehabilitace 3
authors:
  - acluxace
date: 2013-10-03T23:31:58.000Z
tags:
  - regular
---
<p>Pravidelné cvičení zapříčinilo rozhýbání ruky do víceméně libovolné polohy. Bohužel zatím ne vlastní silou. Ruku dokážu pomalu zvednout až nad hlavu. Ale svaly dokáží paži zvednout maximálně 90° od těla při předpažení a asi 70° při upažení.</p>

<p>Velmi důležité je nesnažit se ruku dostat výš posunutím těla. Pohyb musí vycházet jen z ramene. Zdravý člověk si ten <em>špatný</em> pohyb může vyzkoušet zvednutím <em>hodně</em> těžkého předmětu. Tehdy si taky bude pomáhat zádama a celkově zbytkem těla.</p>

<p>Rehabilitace cvičením mi přijdou hodně užitečné a upřímně jsem rád za veškerou tu bolest, kterou způsobují. Poničené tkáně se brání pohybům a bolí to. Ale kde to bolí, tam se to protahuje, uzdravuje.</p>

<p>Perfektní efekt rehabilitací je ten, že celý den se pak cítím skvěle. Tím si i dovolím víc věcí a nemocnou pracičku víc zapojím do běžného používání.</p>