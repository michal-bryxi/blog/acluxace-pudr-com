---
title: Den 07
authors:
  - acluxace
date: 2013-08-03T09:36:16.000Z
tags:
  - regular
---
<p>Spánek je stále poměrně komplikovaná záležitost. Několikanásobné buzení kvůli nepohodlné pozici ruky, bolesti při přetáčení, ztuhlé svaly od strnulé polohy. Spaní se stává nejmíň příjemnou částí dne.</p>