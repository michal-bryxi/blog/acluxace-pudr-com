---
title: Den 159 - Vyndání stehů
authors:
  - acluxace
date: 2014-01-05T14:51:50.000Z
tags:
  - regular
---
<p>Rána byla celou dobu klidná, bez problémů. Hybnost v rameni byla <em>24 hodin</em> po zákroku obnovena. Žádná velká tragédie se s rukou jako v případě první operace neděla. Prášky na bolest, které jsem dostal při odchodu z nemocnice jsem ani nemusel rozbalovat.</p>

<p>Dobrou zkušeností mi je, že když půjdu do lékrány pro &ldquo;náplasti s polštářkem, které mají asi 5 centimetrů&rdquo;, tak musím zdůraznit, že rozměr má mít polštářek a nikoli celá náplast.</p>

<p>Necelé dva týdny po vynětí implantátů z ramene jsem se dostavil na plánované vyndání stehů. Čekárna chirurgie odpoledne den před silvestrem zela prázdnotou. Za pět minut jsem byl venku, kontrola jen v případě komplikací.</p>

<p>Teď už mě čeká jen nějaké to uplatňování nároků na mé pojišťovně, na pojišťovně viníka a jedna série rehabilitací.</p>