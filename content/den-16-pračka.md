---
title: Den 16 - pračka
authors:
  - acluxace
date: 2013-08-10T17:25:00.000Z
tags:
  - regular
---
<p>Obvykle svůj vlastní zápach nevnímáte. A hádám, že matka příroda pro tenhle koncept měla svůj důvod. Co se ale stane když smrdíte, víte že smrdíte, ale nemůžete s tím nic udělat? Celkem osobní neštěstíčko. Dnes ráno jsem se rozhodl, že tu ortézu prostě hodím do pračky a uvidím. 30 stupňů, samostatná várka, poloviční plnění, bez aviváže.Výsledek je uspokojivý. Po zkušenostech bych doporučil následující postup:</p>

<ul><li>Prát klidně na 60°. Vážně by mi vadilo, kdyby ortéza vybledla, nebo se na ní udělaly žmolky?</li>
<li>Ortéza z pračky vyjde překvapivě suchá. Není potřeba ji nechat schnout celý den. Což je i prima trik, pokud v ní zmoknete. Prostě ji dejte vyždímat.</li>
<li>Nebál bych se kapky aviváže. Řekl bych, že to modré brnění už nepáchne jako čivava vytažená z kravského lejna, ale zrovna příjemný čmuch to taky nemá.</li>
<li>Nejdůležitější je vzít všechny suché zipy a zalepit je &ldquo;do sebe&rdquo;. Pak z pračky nevyndáte jen zašmodrchanou kuličku jako já.</li>
</ul><h3>Rameno</h3>

<p>Spaní už je řekněme uspokojivé. Probouzím se maximálně jednou za noc a přes šest hodin spánku, už to možná taky bude. Jak se postupně rameno hojí, tak je méně situací, kde si způsobím bolest neopatrným pohybem. Nicméně nešikovnost ortézy je pořád stejná. Vykouzlit i tak primitivní jídlo jako jsou <em>míchaná vajíčka</em> je prostě problém. Moc se těším na sundání ortézy.</p>