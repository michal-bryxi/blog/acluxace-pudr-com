---
title: Den 04 - vizita
authors:
  - acluxace
date: 2013-07-30T20:05:00.000Z
tags:
  - regular
---
<h3>06:00</h3>

<p>Budíček, snídaně, vizita, propuštění do domácího léčení. Za 4 dny se mám dostavit na převaz. Poránu mě celkem nepěkně škubalo v operované paži. Něco při jistých pozicích těla brnkalo na nervy.</p>

<h3>08:00</h3>

<p>Odjezd do práce - vyzvednout si notebook.</p>

<p>Poté jsme se stavili u obvoďačky. Otevřela zdravotní sestra, koukla na ortézu a klidně se zeptala: &ldquo;Kolo?&rdquo;. Se smíchem jsem se jí zeptal jak to uhodla. Opět nevzrušeně, jednoslovně odpověděla: &ldquo;Praxe&rdquo;. Obvoďačka se mi snažila vysvětlit co mám kam donést za papír, kam se mám kdy dostavit. Obávám se, že jsem chytl sotva polovinu. Stonat v tomhle státě je nemalá byrokracie. Přehlásil jsem si místo pobytu na nemocenské mimo místo trvalého pobytu. Bez nesmyslného papírování to lze jen na tři dny.</p>

<p>Volal jsem na policii ohledně výslechu. Bylo mi sděleno že praporčík který má případ se mnou řešit je na dovolené a mám si zavolat za 8 dní. Bezva.</p>

<h3>13:00</h3>

<p>Odpoledne a večer klidový režim. Hodně často se dostavují křeče. Už nevím jak si lehnout, abych si ulevil. Po dlouhém zkoumání jsem přišel na to, že mě jen škrtí ortéza. Při příchodu křeče stačí ortézu prstem u krku nadzvednout a je klid.</p>