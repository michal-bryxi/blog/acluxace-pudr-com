---
title: Den 147 - Vyndání implantátů
authors:
  - acluxace
date: 2013-12-22T14:07:00.000Z
tags:
  - regular
---
<h4>Přípravy</h4>

<p>Velmi brzy ráno jsem se dostavil na oddělení dětské ortopedie, kde proběhla rutinní kontrola již zkontrolovaných papírů. Odtud mě poslali do jakési kanceláře pro příjem do nemocnice. Tam jsem nahlásil adresu, bydliště, telefon, kontakt na člověka který mě bude moci odpojit od přístrojů pro případ že se <em>něco nepovede</em>. Dále už jen na kliniku <em>traumatologie pohybového ústrojí</em>, kde jsme se na chodbě jako skupinka nově příchozích postupně vyměnili s již odoperovanými pacienty. Jeden odešel domů, a jiný se převlékl do pyžama a byl uložen na lůžko. Bylo nás celkem šest.</p>

<p>Postupně jsme si všichni popovídali s doktorem a anesteziologem. Byl jsem jediný, který přišel s ramenem. Všichni ostatní měli problém s kolenem. Asi právě proto jsem přišel na řadu jako poslední, přibližně kolem poledne.</p>

<h4>Operace</h4>

<p>Včelička do zadku a sestřička už jede se mnou a s mojí postelí kamsi do útrob nemocnice. V předsálí cílové místnosti mě nechají přelézt na pojízdný operatérský stůl a při tom vtipkují: &ldquo;Pokud to sám nezvládnete přelézt, tak vás nemůžeme operovat&rdquo;.</p>

<p>Jedeme na sál. Tady mě opět zdraví operatér a vysvětluje mi, že použijí část již existující jizvy na vytažení výztuh. Poté přijde anesteziolog a poví mi pouze: &ldquo;Dobrou noc&rdquo;. Poprosím je ještě, aby mi udělali pěknou jizvu, na což doktor reaguje: &ldquo;Na chlapech jsou všechny jizvy pěkný&rdquo;. Kolotoč a spánek.</p>

<h4> Po operaci</h4>

<p>Probudím se až zpátky na lůžkovém oddělení. Sestřička mi leduje rameno a neustále mi nabízí léky proti bolesti. Odmítám. Žádné bolesti narozdíl od těch kteří přišli s kolenem nemám. Nakonec stejně přijde a &ldquo;drogy&rdquo; mi aplikuje s tím, že si prostě musí udělat čárku. Operace hybnost ramene nijak významně neomezila. Jen si musím dávat trochu pozor, abych pohybem oblečení netahal za náplast/stehy.</p>

<p>Během předoperačních rozhovorů s doktory jsem několikrát žádal o to, aby mi vyoperované implantáty z ramene zabalili sebou. Sami mi to vlastně nabídli. Neměl jsem tedy moc ponětí co bych s tím dělal, ale jako suvenýr mi to přišlo super. Bohužel po probuzení jsem se od sestřičky dozvěděl, že se mnou žádný balíček neposlali, takže mám smůlu.</p>

<p>Ráno přijde doktor, koukne na sešitou ránu a propustí mě do domácího léčení. Před lůžkovým oddělením ještě potkám známou, která se z lyží vrátila o berlích rovnou do nemocnice.</p>