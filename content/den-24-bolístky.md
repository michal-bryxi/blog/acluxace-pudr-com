---
title: Den 24 - bolístky
authors:
  - acluxace
date: 2013-08-18T21:31:00.000Z
tags:
  - regular
---
<p>Zápěstí fixované ruky je už tak pohmožděné, že se nedá rozumně rozcvičit. Preventivně nosím ortézu na předloktí, aby nebolelo a jeho stav se nezhoršoval.</p>

<p>Díky geniálnímu tvaru ortézy na rameno jsem si něco provedl se svalem pod pravou (zdravou) lopatkou a ta mě bolí při každém pohybu. Jediná úlevová poloha je &ldquo;hande hoch&rdquo;, kterou ale ze zřejmých důvodů nelze provozovat celý den.</p>

<p>Nejdelší souvislý spánek za posledních pár dní odhaduji na 3 hodiny. Z noci na dnešek jsem byl alespoň <em>6x</em> vzhůru a vždy musel rozpohybovat nějakou tu <em>bolístku</em>.</p>

<p>Spaní v ortéze přes rameno se jeví jako nemožné. Řeže mne do lopatky a minimálně v půlce noci ji ze sebe prostě musím servat. Nezdá se mi už pravděpodobné, že bych si ve spánku mohl ublížit.</p>

<p>Spaní s ortézou na předloktí zase vytváří nepříjemné otlačeniny. Bez ní je ale ráno silná bolest v zápěstí.</p>

<p>Spánek se stal nejmíň oblíbenou součástí dne.</p>