---
title: Den 50 - rehabilitace 1
authors:
  - acluxace
date: 2013-09-13T19:54:44.000Z
tags:
  - regular
---
<p>První rehabilitace. Plavky, ručník, dobropisy do batůžku a jdeme. Nejdřív mě ponořili do vířivky. Bublinky se na rameno prakticky nedostaly. Ale na pohmožděné zápěstí to bylo hodně příjemné. <em>30</em> minut byla podle mě nesmyslně dlouhá doba na vířivku. Příště si vezmu knížku.</p>

![Vířivka](/images/den-50-rehabilitace-1.jpg)

<p>Druhá část bylo fyzické cvičení. To mám na rehabilitacích nejraději a přijde mi nejvíce efektivní. Slečna rehabilitační pracovnice byla celkem od rány a byla potěšená, že se mnou nemá tolik práce. Prý na tom nejsem tak zle. Několik postřehů:</p>

<ul><li>Pokud ležím na zádech, jsem už schopný nemocnou ruku dát nad hlavu. Gravitace pomáhá.</li>
<li>Pohyb ruky je hodně ovlivněn pohybem lopatky. Když se na to člověk soustředí, zjistí jak mu tam různě jezdí.</li>
<li>Pokud si nedávám pozor, tak pohyb nemocné ruky přetahuji do pohybu ramene, nebo celého hrudníku. Což je špatně.</li>
<li>Vyprsit se! Ramena dozadu!</li>
<li>V zápěstí je spousta kostiček. Pokud nejsou šlachy/svaly v zápěstí řádně trénované, kostičky cestují. A z toho jsou pak velké bolesti. Pomáhá jakékoli trénovátko prstů: pěnový balónek, gumový kroužek, posilovátko prstů.</li>
<li>Cvičení mi bylo doporučeno alespoň <em>3x</em> denně.</li>
<li><em>Jeden týden</em> nepoužívání svalu se zpátky vycvičuje cca <em>měsíc</em>.</li>
</ul>
