---
title: Den 70 - posudek na kolo
authors:
  - acluxace
date: 2013-10-06T23:31:53.000Z
tags:
  - regular
---
<p>Nechce se mi. Opravdu se mi nechce spoléhat na to, že rám byciklu jemuž bylo nárazem ohnuto přední kolo do &ldquo;L&rdquo; je stále ve stoprocentní kondici. Proto jsem se zaradoval, když přišlo vyjádření z cykloservisu.</p>

<p>Stručně řečeno nedoporučují provozování rámu kola, které prošlo takto závažnou nehodou. A navrhli jeho výměnu. Celková částka opravy je odhadována na cca <em>90%</em> původní ceny kola.</p>

<p>Současně dnes přišel dopis od společnosti zastupující mé zájmy ve kterém žádají o aktualizování jakýchkoli budoucích nových informací. Podle popisu se zdá, že jakýkoli náklad přímo spojený s nehodou je možné připsat na účet. I lístky na MHD a regulační poplatky. To mi přijde rozumné.</p>