---
title: Den 98 - nové rehabilitace
authors:
  - acluxace
date: 2013-12-09T09:01:45.000Z
tags:
  - regular
---
<p>Poslední rehabilitace mi skončila cca před týdnem. Poté se dává pacientovi asi 10 dní pauza, aby se dostal do svého &ldquo;normálního&rdquo; stavu. Dva dny po rahbilitaci se vždy cítím výborně a nic mě nebolí. Takže, dělat kontrolu okamžitě by nedávalo moc smysl. Dnes jsem měl pohovor s doktorkou na téma dalšího plánu mého léčení.</p>

<p>Podle pravdy jsem nahlásil, že rameno už vypadá velmi dobře, ale že jsou velké problémy se zápěstím. Doktorka mi ho lehce prohmatala. Koukla se jak hezky umím dát obě ruce nad hlavu, aniž bych se zlomil v pase na stranu. Sepsala zprávu a předala mi poukázku na dalších 8 lekcí v jejich zařízení. Budu chodit do vířivky, na fyzické cvičení a na magnetoterapii.</p>