---
title: Den 03 - nuda
authors:
  - acluxace
date: 2013-07-30T10:33:00.000Z
tags:
  - regular
---
<p>Ráno přišel doktor a udělal převaz. Vyndal mi zpod ortézy spoustu vatiček a obvazů o kterých jsem do té doby nevěděl. Důrazně nedoporučovalo s rukou rotovat.</p>

<p>Bolení ramene je pořád stejné - přijatelné. Snažím se pendlovat po chodbách, abych trénoval vzpřímený postoj a nekřivil si záda.</p>

<p>Mytí je s ortézou vážně problém. Není možné se pod ní dostat. Současně jedna ruka neumožňuje dosáhnout na všechna místa na tělě. Ruka se v ortéze silně potí a zasloužila by si omýt.</p>

<p>Už jsem  ve stavu, kdy bych sám sebe propustil domů, ale to může udělat jen primář a ten tu bude až zítra.</p>