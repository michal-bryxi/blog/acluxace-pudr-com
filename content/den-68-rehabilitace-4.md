---
title: Den 68 - rehabilitace 4
authors:
  - acluxace
date: 2013-10-05T23:31:29.000Z
tags:
  - regular
---
<p>Sedím v čekárně nemocničního zařízení a čekám na vířivku. V chodbě skotačí celá školka dětí čekajících na to samé a snaží se překřičet hypotetické kolem přistávající letadlo. Ve změti hlasů vyslechnu monolog vychovatelky k holčičce:</p>

<blockquote>
  <p>No to si ze mě děláš srandu Aničko. Teď jsme byli ve školce, 
  kde jsi se mohla vyčůrat a tobě se chce zase? Tak to teda ne. 
  To budeš muset vydržet, jsi už velká holka.</p>
</blockquote>

<p>Asi nemusím zdůrazňovat jak moc jsem si přal, abychom nesdíleli stejnou vířivku.</p>

<h3>Rehabilitace</h3>

<p>Jako vždy jsem dostal pochvalu, že se se mnou dobře pracuje a že jsem hezky pohyblivý. Moc se nedivím že oproti seniorům, kteří jsou častější návštěvníci těchto zařízení se může můj stav zdát skvělý.</p>