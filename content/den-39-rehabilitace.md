---
title: Den 39 - Rehabilitace
authors:
  - acluxace
date: 2013-09-03T21:35:03.000Z
tags:
  - regular
---
<h4>Rehabilitační námluvy</h4>

<p>Rehabilitační pobočka pár set metrů za barákem. Setkání se sestřičkou. Vysvětlení problému. Uvidíme se ve <em>dnu 46</em>.</p>

<h4>Okresní správa sociálního zabezpečení</h4>

<p>Dostal jsem dopis od <em>OSSZ</em>, který mi upřímně přišel jako vtip. Vyhrožuje tím, že bez vyplnění přiloženého formuláře mi nebude přiznána nemocenská. Nabízí v checkboxech možnosti, které jsou všechny ekvivalentem střelení se do nohy. A chce po mě, abych formulář odeslal na Pražskou pobočku, ale příslušnou adresu nepřikládá.</p>

<p>Došel jsem si tedy na Plzeňskou pobočku OSSZ, kde mi zmatená paní v podatelně napůl hlavy odkývala, že teda tady ten formulář můžu nechat a bude to v pořádku.</p>

![Dopis od OSSZ](/images/den-39-rehabilitace.jpg)
