---
title: Den 15 - Převaz II
authors:
  - acluxace
date: 2013-08-10T17:06:00.000Z
tags:
  - regular
---
<p>Ranní skákání jsem raději nechal těm, kteří měli šanci se v noci pořádně prospat. Koneckonců prožít si dvouhodinový &ldquo;onhold&rdquo; maraton v čekárně nemocnice můžu klidně odpoledne. Pomocí MHD jsem se překvapivě pohodlně přesunul k Fakultní nemocnici Lochotín a cca v deset hodin jsem vchodem F vstupoval dovnitř.</p>

<p>Vyšlapat do patra na kliniku <em>ortopedie a traumatologie pohybového ústrojí</em>. Všimnout si na dveřích plakátku o regulačních poplatcích. Naštvaně zdupat zase dolů. Koupit známku. Nahoru. Odevzdat lístek sestřičce. Podivit se rychlému odbavení (cca 45 minut). Vstoupit do ordinace.</p>

<p>Doktor mi k mému překvapení sdělil, že budeme vyndávat stehy. Původně jsem si myslel že to bude někdy kolem čtvrtého týdne a tohle bylo teprve 14 dní od operace. Nu, bránit se nebudu. Zatímco jsem se snažil ze sestřičky vymámit jestli ortézu prát na 30, nebo s předpírkou, doktor odstraňoval jednotlivé stehy. Moc se s tím nepáral. Na mojí otázku: &ldquo;Kolik jich tam celkem je?&rdquo; Odověděl že hodně. Škoda. Celkové počítání zářezů holt budu muset někdy se zrcátkem provést sám. Sestřička se mezitím neurčitým: &ldquo;Já vám nevim, jestli se to dá prát&rdquo; vyhla mé odpovědi ohledně údržby ortézy. Pomyslel jsem si, že dnes se asi nic kloudného nedozvím.</p>

<p>Poměrně zajímavé zjištění bylo, že mám odteď ortézu rozepínat a bez rotace natahovat loket, aby nezdřevěněl. Něco takového jsem dělal už hodně, hodně dávno. Nějaké protažení jsem viděl jako nutnost.</p>

<p>Krom posunutí termínu vyndávání stehů se mi také šouplo sundání ortézy. Ode dneška za čtrnáct dní - den 29. Bezva.</p>

<p>Odevzdat známku. Nechat si pomoci do trička. Kouknout, jak se <em>správně</em> nasazuje ortéza. Říci pápá.</p>

<h3>Inženýr</h3>

<p>Sestřička mě do ordinace vždy volá jako: &ldquo;Pan <em>inženýr</em> Bryxí&rdquo;. Ble. Příště si musim dát pozor co si v nemocnici opisujou. Při tomhle oslovování si pokaždé připadám jak namyšlenej pitomec.</p>