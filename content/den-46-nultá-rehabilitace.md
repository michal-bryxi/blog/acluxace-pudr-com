---
title: Den 46 - nultá rehabilitace
authors:
  - acluxace
date: 2013-09-10T16:24:27.000Z
tags:
  - regular
---
<p>Nejsem člověk, který by zářil schopností věci dobře naplánovat a skvěle časově zvládnout. Ale předvídavost a plánování našeho zdravotnictví mě stále docela zaráží.</p>

<ul><li>Ode dne 0 (46 dní zpět) je asi všem lékařům kteří mě potkali jasné, že budu potřebovat rehabilitace.</li>
<li>Ve dnu 29 - po sundání ortézy doktor prohlásil že to asi bude chtít rehabilitace. Začínám sám cvičit.</li>
<li>Ve dnu 33 jsem opravdu dobropis na rehabilitace dostal.</li>
<li>Ve dnu 39 jsem se domlouval přímo na rehabilitačním pracovišti na tom, že budu něco potřebovat.</li>
<li>Ve dnu 46 mě na rehabilitacích zkoukli a sepsali rehabilitační plán.</li>
<li>Ve dnu 50 půjdu prvně na rehabilitace.</li>
</ul><p>S rehabilitační sestřičkou jsme měli celkem problém najít nějaké rozumné termíny. Takže prvních 7 návštěv mám víceméně někde uprostřed dne. Zbylé tři už jsem si mohl vybrat pěkně. Vidím tu jistou možnost zlepšení plánování rozvrhu rehabilitací. Času na to bylo opravdu dost.</p>

<p>Podle názoru doktorky deset návštěv na takové poranění stačit nebude. To mě celkem děsí.</p>