---
title: Den 71 - rehabilitace 5
authors:
  - acluxace
date: 2013-10-07T23:30:58.000Z
tags:
  - regular
---
<p>Průběh sezení na rehabilitacích je vždy stejný. Vířivka a poté cvičení s pomocí sestřičky. Občas mám pocit, že do vany s bublinkami lezu hlavně proto, aby rehabilitační pracovnice nemusela sahat na zpocené pacienty. Fyzický přínos perličkové lázně pro můj handikep bych rozporoval.</p>

<p>Ze začátku jsme dělali hlavně protahovací cviky, abychom ukázali svalům co se bude dít. Teď už jsou sestavy zaměřeny hlavně na protažení svalů v pozicích, které bolí. A na <em>posilování</em>.</p>

<h3>Cviky</h3>

<p>U všech cviků je důležité kontrolovat pohyb proti zdravé ruce. Ta ho udělá instinktivně správně. Stejně tak je nutné neprohýbat se v zádech. To je lidský instinktivní &ldquo;úskok&rdquo; ve chvíli, kdy <em>už nemůžeme</em>. Ničíme si tím ale jinou partii svalů.</p>

<h4>Boxování vleže</h4>

<p>Ležím na zádech, ruce podél těla, pokrčené v loktech, dlaněmi směrem k hlavě. Paže <em>boxují</em> směrem vzhůru ke stropu a cestou se přetočí tak, aby dlaně směřovaly k nohám.</p>

<p>Cvik jsem musel od druhé lekce obohatit o <em>2kg</em> činky. Není náročný. Udělat 20 opakování je bez problému. Vzhledem k mému zlobivému zápěstí je vytočení dlaní směrem k hlavě bolestivé, takže se musím přemáhat, abych cvik dělal pořádně.</p>

<h4>Andělíčci</h4>

<p>Cvik na zádech. Ve výchozí poloze jsou ruce podél těla na podložce. Zvedneme paže mírně nad podložku a děláme &ldquo;andělíčky&rdquo; do upažení, nebo trochu výš - podle možností. Opět 20 opakování není problém, pokud nepoužívám činky. Vždy dělám 3 série s drobnými obměnami - dlaně nahoru, palce nahoru, hřbety rukou nahoru.</p>

<h4>Protahování &ldquo;za hlavou&rdquo;</h4>

<p>Ležíme na zádech. Ruce předpažené, ukazující gesto &ldquo;thumbs up&rdquo;, vzájemně se dotýkající špičky palců obou rukou. Pomalu se paže pokusíme dostat co nejvíce nad hlavu - do vzpažení. Ve chvíli kdy nemocná ruka nemůže, zhluboka vydechneme a zatlačíme ji přes bolest o kus dál. Opakujeme cca 5x, nebo než se dostaví křeč. Po tomto cviku si musím promasírovat biceps, který je obvykle smrštěn v oné křeči.</p>

<h4>Protahování zápěstí</h4>

<p>Jednoznačně nejbolestivější cvik, který potřebuju provádět. Díky pochroumanému zápěstí je jakékoli zapojení levé horní končetiny do všedních činností prostě problém. Jakékoli zatížení zápěstního kloubu na krut/ohyb je nesnesitelné.</p>

<p>Proto je potřeba jít bolesti vstříc a vyvrátit zápěstí dopředu/dozadu. Stejně jako při protahování rukou až do bolesti, zatnout zuby, dlouhý výdech a tlačit i přes bolest dál.</p>

<h4>Dámské kliky</h4>

<p>Přiznávám. Používat množné číslo (kliky) pro jeden až dva kusy (kliky) je trochu luxus. Záda by tu &ldquo;zátěž&rdquo; sice už unesla, ale opřít se o zápěstí pořádně nedokážu. Každopádně i v tom malém počtu je u kliků krásně cítit, jak se svaly protahují.</p>