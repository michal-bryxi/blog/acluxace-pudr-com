---
title: 'Den 81 - rehabilitace 6, 7, 8'
authors:
  - acluxace
date: 2013-10-15T08:01:44.000Z
tags:
  - regular
---
<h4>Zlobivé zápěstí</h4>

<p>Nic není v této tragikomické části mého života tak otravné jako moje pohmožděné zápěstí. Ve dnu <em>75</em> pozoruji citelné zlepšení síly zápěstí. Svaly regenerují. Pěnový balonek téměř i zvládám zmáčknou do placata. Den <em>78</em> a před rehabilitací mi přijde, že mi ruka snad upadne. Všech osm kostiček v zápěstí tancuje sem a tam. Musím je pravidelně &ldquo;nahazovat&rdquo; zpátky na místo. Stisk zápěstí je samozřejmě ten tam. Zkuste si tlačit na nůž, který vás řeže do ruky. Moc se vám chtít nebude. Stejně tak mě se nechce ruku používat.</p>

<p>Je to trochu paradox. Aby mě pohyb nebolel, tak necvičím s míčkem. Když necvičím s míčkem, tak svaly ochabnou. Jakmile svaly ochabnou, ruka bolí daleko víc. A tak hezky dokola.</p>

<p>Sám jsem si tedy naordinoval spaní v <a href="https://www.google.cz/search?q=ort%C3%A9za+p%C5%99edlokt%C3%AD&amp;client=ubuntu&amp;hs=ht6&amp;channel=cs&amp;tbm=isch&amp;tbo=u&amp;source=univ&amp;sa=X&amp;ei=nBZcUvjoI4XFtAahm4H4CQ&amp;ved=0CC4QsAQ&amp;biw=1536&amp;bih=812">ortéze na předloktí</a>. Zápěstí je zafixované jako miminko v zavinovačce a překvapivě se hned po první noci ráno probouzím s mnohem použitelnějším pařátkem. Další dny se situace stále zlepšuje až v dnu <em>81</em> zkouším místo pěnového míčku cvičit s <a href="https://www.google.cz/search?q=gumov%C3%BD+krou%C5%BEek+prsty&amp;client=ubuntu&amp;hs=9dR&amp;channel=cs&amp;source=lnms&amp;tbm=isch&amp;sa=X&amp;ei=1hdcUvWhK83Hsgasi4GQBQ&amp;ved=0CAkQ_AUoAQ&amp;biw=1536&amp;bih=812">gumovým kroužkem</a>. Guma se při maximálním úsilí sotva pohne, ale už samotnou výměnu cvičebního nástroje považuju za úspěch.</p>

<h4>Rameno</h4>

<p>Viditelnější část úrazu - rameno - zdá se nebude úplně problém. Minimálně z něj mám dobrý pocit. Cvičí se čím dál lépe. Pohyblivost je po rozcvičení v obou rovinách téměř <em>180°</em>. Krajní polohy stále nepříjemně tahají a za svůj rozsah se nedostanu ani kdybych to chtěl &ldquo;přebejčit&rdquo;.</p>

<p>Zkoušel jsem se pověsit plnou vahou, oběma rukama na hrazdu. Dobrý. Rameno mi neupadlo. Přitáhnutí jsem pak zkoušel jen chvíli/trochu. Síla by asi byla, ale z želez v rameni mám nepříjemný pocit. Něco se tam o něco jiného dře.</p>

<p>Všechny dříve popisované cviky zvládám už bez problémů. A to i s <em>činkou - dvojkou</em>. Ve dnu <em>81</em> jsem od rehabilitační pracovnice dostal nakázáno dělat stejnou sestavu cviků, ale vsedě. Gravitace v téhle pozici bude pracovat proti mně, místo toho aby mi pomáhala.</p>

<h4>Pojišťovna</h4>

<p>Přišlo mi vyjádření od pojišťovny policie ČR:</p>

<blockquote>
  <p>bla bla bla &hellip;</p>
  
  <p>Pojistné plnění ve výši skutečné škody: XXXX,- Kč</p>
  
  <p>Výše pojistného ručení byla stanovena na základě prohlídky a provedené fotodokumentace poškození.</p>
  
  <p>Souhlas s rozsahem poškození je potvrzen podpisem poškozeného na protokolu o prohlídce.</p>
  
  <p>Servisní zprávu o rozsahu poškození je možno posuzovat jako účelovou.</p>
  
  <p>Z tohoto důvodu byla výše škody určena rozpočtem na opravu předního kola s výměnou ráfku a výpletu,</p>
  
  <p>výměnou gripů, košíku na lahve a pedálů včetně času, stráveného výměnou.</p>
</blockquote>

<p>Takže to chápu jako vzkaz: Milý kliente. Hodili jsme na tebe práci se zjištěním rozsahu závad pomocí experta. Jeho vyjádření se nám ale nelíbilo, takže si z něj vytáhnem jenom tu část, která nám přijde prima. Jo a kdyby jsi pochyboval, tak tady je papír který jsi podepsal a souhlasil s tím, že tak to můžem my (pojišťovna) udělat.</p>

<ol><li>Ano, souhlas s rozsahem jsem dal. Ale likvidátor mi (per huba) potvrdil že papír není úplný a mám si zařídit doprohlídku.</li>
<li>Papír mnou podepsaný likvidátorovi jasně říká:

<ul><li>Poškození výše nepopsaná nebo <strong>jinak nedoložená</strong> nebudou zahrnuta do výpočtu pojistného plnění.</li>
</ul></li>
</ol><p>Obratem jsem poslal popis této události společnosti, která zastupuje moje zájmy. Jsem zvědavý na jejich vyjádření.</p>