---
title: Den 05 - ortéza
authors:
  - acluxace
date: 2013-07-31T10:14:00.000Z
tags:
  - regular
---
<p>Ortéza, kterou používám musel navrhovat nějaký bělovlasý, šílený vědátor s tlustými brýlemi a bílým pláštěm. V současných vedrech v ní funguju jako levná továrna na výrobu koncentrovaného lidského potu. Na torzu to nemá jediný větrací otvor. Údajně to je vyrobené z neoprenu. Povrchová úprava heboučký plyšáček.</p>

<p>Zafixovaná ruka má ve všech případech (vleže, ve stoje, v papasanu, na židli, vzhůru nohama) nepřirozenou polohu. Je nějak vyvrácená. V ortéze ji není možné pořádně zafixovat do <em>přímé</em> polohy. A ani ruka sama se nemá čeho chytnout, takže často prostě podlehne gravitaci a visí směrem k zemi.</p>

<p>Samotnou kapitolou je pak ležení. Když si lehnu na záda na rovnou podložku, levou ruku pokrčím v lokti a obejmu jí pravý bok, pak mám předloktí přesně v místech, kde končí žebra. Na hraně. Mám dost propadlé břicho proti hrudníku. Hrudní koš tlačí na ruku a umrtvuje ji. Ruka nepříjemně tlačí na hrudník. Zápěstí jako vždy v ne úplně ideální poloze. Spánek je možný, ale doposud přinejlepším se dvěma pauzama na zdravotní procházku.</p>

![Já a moje přítulná společnice](/images/den-05-orteza.jpg)

<h3>Policie</h3>

<p>Volala mi policie. Pravděpodobně to vážně vyšetřují dvě oddělení, protože z rozhovoru jsem nabyl dojmu, že policista, který má být na dovolené není vůbec součástí toho o čem jsme se teď po telefonu bavili. Dohodli jsme se na tom, že ve <em>dnu 08</em>, až půjdu na převaz mi zavolají a dohodnem si pořádně termíny výslechu.</p>
