---
title: Den 21 - dopis od pojišťovny
authors:
  - acluxace
date: 2013-08-16T08:08:00.000Z
tags:
  - regular
---
<p>Přišel dopis od mé pojišťovny, který mi přátelskými, malými písmenky vysvětluje, že by rádi věděli kdo mi co udělal a že je v mém zájmu, aby se to dozvěděli. Aby oni mohli pro sebe vymoci peníze z pojišťovny viníka. Krom nikde nezmíněné varianty, že jinak budou peníze vymáhat po mě tu žádný vlastní přínos ve vyplnění dotazníku nevidím.</p>

<p>Dotazník vymýšlel nějaký UX génius.</p>

<ul><li>Dotazník jakožto i volná místa jsou psány cca &ldquo;dvanáctkou&rdquo; písmem. Moje běžné, již tak drobné písmo prostě nemá šanci se do připravených řádků pohodlně vejít.</li>
<li>Na otázky typu <em>ano / ne</em> je často vynechán celý řádek.</li>
<li>Pro &ldquo;Jak k úrazu došlo - podrobný popis&rdquo; jsou vyhrazeny řádky dva. Takže jsem se (nadneseně) omezil jen na: &ldquo;Jedu na kole, auto v cestě, bác!&rdquo;</li>
<li>Otázky velmi často směřují na věci, které zatím nemám šanci vědět. Například o poskytnuté rahabilitační léčbě. Odpovědi na ně jsem vždy uvodil slovy: &ldquo;předpokládám že&hellip;&rdquo;</li>
<li>Na případný nákres dopravní nehody mi bylo vymezeno místo cca <em>7x7 cm</em></li>
</ul><p>Alespoň že je přiložena obálka se známkou.</p>

<h3>Zápěstí</h3>

<p>Zápěstí pochroumané ruky opravdu hodně trpí dlouhodobým setrváním v nepřirozené poloze. Pomáhá mít celý den zatnutou pěst a tím zápěstí srovnat. Nebo celý den mačkat <a href="https://www.google.cz/search?q=trenovac%C3%AD+krou%C5%BEek+na+prsty&amp;client=ubuntu&amp;channel=cs&amp;gws_rd=cr&amp;um=1&amp;ie=UTF-8&amp;hl=en&amp;tbm=isch&amp;source=og&amp;sa=N&amp;tab=wi&amp;authuser=0&amp;ei=ls4NUu3IIIy7hAeks4GABg&amp;biw=1535&amp;bih=812&amp;sei=mc4NUvfqEeWu0QWQ44DYBw#authuser=0&amp;bav=on.2,or.r_cp.r_qf.&amp;channel=cs&amp;fp=d24f097c118b0a5b&amp;hl=en&amp;q=posilov%C3%A1n%C3%AD+prst%C5%AF&amp;sa=1&amp;tbm=isch&amp;um=1">trénovací kroužek na prsty</a>. Nejjednodušší pomoc je prostě si k <a href="https://www.google.cz/search?q=trenovac%C3%AD+krou%C5%BEek+na+prsty&amp;client=ubuntu&amp;channel=cs&amp;gws_rd=cr&amp;um=1&amp;ie=UTF-8&amp;hl=en&amp;tbm=isch&amp;source=og&amp;sa=N&amp;tab=wi&amp;authuser=0&amp;ei=ls4NUu3IIIy7hAeks4GABg&amp;biw=1535&amp;bih=812&amp;sei=mc4NUvfqEeWu0QWQ44DYBw#authuser=0&amp;bav=on.2,or.r_cp.r_qf.&amp;channel=cs&amp;fp=8f7d530c8332679a&amp;hl=en&amp;q=ort%C3%A9za+na+rameno&amp;sa=1&amp;tbm=isch&amp;um=1">ramenní ortéze</a> ještě vzít <a href="https://www.google.cz/search?q=trenovac%C3%AD+krou%C5%BEek+na+prsty&amp;client=ubuntu&amp;channel=cs&amp;gws_rd=cr&amp;um=1&amp;ie=UTF-8&amp;hl=en&amp;tbm=isch&amp;source=og&amp;sa=N&amp;tab=wi&amp;authuser=0&amp;ei=ls4NUu3IIIy7hAeks4GABg&amp;biw=1535&amp;bih=812&amp;sei=mc4NUvfqEeWu0QWQ44DYBw#authuser=0&amp;bav=on.2,or.r_cp.r_qf.&amp;channel=cs&amp;fp=64b62a07a4ba178b&amp;hl=en&amp;q=ort%C3%A9za+na+z%C3%A1p%C4%9Bst%C3%AD&amp;sa=1&amp;tbm=isch&amp;um=1">ortézu na zápěstí</a>, která ho stabilizuje v optimální poloze. Hmm, ortéza v ortéze.</p>