---
title: Den 41 - Procvičování
authors:
  - acluxace
date: 2013-09-04T09:02:18.000Z
tags:
  - regular
---
<p>S dopomocí druhé ruky jsem schopný tu nemocnou dostat až <em>40°</em> od těla. V této pozici už je cítit tah v rameni. Něco zrezlého nepoužíváním.</p>

<p>Hezký cvik je sednout si na zem do tureckého sedu, položit ruce před sebe na zem. A pomalu je posouvat dopředu jakoby se člověk chtěl čelem dotknout země. Zatím se víceméně vůbec nepohnu z místa.</p>

<p>Pozitivní zpráva je, že zápěstí se celkem srovnalo a při pravidelném procvičování s <a href="https://www.google.cz/search?q=ma%C4%8Dkac%C3%AD+balonek&amp;client=ubuntu&amp;channel=cs&amp;um=1&amp;ie=UTF-8&amp;hl=en&amp;tbm=isch&amp;source=og&amp;sa=N&amp;tab=wi&amp;authuser=0&amp;ei=HucmUqfXGYjrswb71IHQCQ&amp;biw=734&amp;bih=819&amp;sei=IecmUtT5McvCtAbnxYHYDQ#authuser=0&amp;channel=cs&amp;hl=en&amp;q=posilovac%C3%AD+m%C3%AD%C4%8Dek&amp;tbm=isch&amp;um=1&amp;imgdii=_">posilovacím míčkem</a> a mazáním už téměř nebolí.</p>