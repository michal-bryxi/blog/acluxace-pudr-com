---
title: Den 29 - rentgen
authors:
  - acluxace
date: 2013-08-26T18:53:36.000Z
tags:
  - regular
---
<p>Konečně v online kalendáři bliknul ten den označený jako &ldquo;sundání ortézy&rdquo;. Ranní přesun do nemocnice. Anabáze shánění regulační známky, když za vrátného zastupuje jen neschopná &ldquo;přijdu hned&rdquo; cedulka. Sehnání schopného záskoku. Koupení známky. Překvapení nad téměř prázdnou čekárnou. Čekání, čekání, čekání.  Přesměrování na rentgen. Blik cvak, rentgen pošlem doktorovi mailem. Čekání, čekání, čekání. Příjem u doktora.</p>

<p>Opět mě obsluhuje jiný pán v bílém plášti. Sestřička to už zabalila, tuším že šla na oběd. Doktor prohlásil, že jizva se hojí pěkně a tahem ruky mi vysvětloval kam ji mám pomáhat já, abych si ji procvičil. Trochu to bolelo.  Ihned jsem pochopil, proč by měla pravačka (ta zdravá) levačce (té nemocné) pomáhat. Levá ruka mi víceméně jen vlaje u těla. Při maximálním úsilí ji dostanu cca <em>30°</em> od těla v kterémkoli směru. A ještě to nepěkně bolí. Nejde ani o to, že by mě nějak zmršila operace. Při dopomoci ruku ohnu do daleko větších úhlů. Jen v ní prostě nemám tu sílu. <em>Měsíc</em> ne-používání tohle způsobí.</p>

<p>Poté doktor zkoumal rentgeny ramene. Uprosil jsem ho, abych si je mohl vyfotit:</p>

![AC luxace před operací](/images/den-29-rentgen-pred.jpg)
![AC luxace po operaci](/images/den-29-rentgen-pote.jpg)

<p>Na mou otázku co s rehabilitacema doktor prohlásil, že: &ldquo;To rozhýbete doma. Tady rehabilitace nemá význam.&rdquo; Ou-kej no. Jen doufám, že to pak nepoznám v části <em>trvalé následky</em>.</p>

<p>Rozloučení. Uvidíme se za <em>6 týdnů</em>. Opouštím nemocnici.</p>

<p>Těšil jsem se na lepší spánek, ale celkem marně. Ruka opravdu bolí v zápěstí. A rameno nedovoluje moc příjemné polohy při mazlení s postelí.</p>
