---
name: Michal Bryxí
id: acluxace
image: /images/michal-bryxi.jpg
website: https://www.pudr.com
twitter: MichalBryxi
facebook: michal.bryxi
location: Edinburgh, Scotland
---

bike 🚴 | run 🏃🏻‍ | climb 🧗 | travel 🌍 | enjoy life 💚

IT guy with the need to live fully.
