import Application from 'unicorn-misa/app';
import config from 'unicorn-misa/config/environment';
import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-qunit';

setApplication(Application.create(config.APP));

start();
