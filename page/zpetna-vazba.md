---
title: Zpětná vazba
image: /images/zpetna-vazba.jpg
imageMeta:
  attribution: Raphael Schaller
  attributionLink: https://unsplash.com/@raphaelphotoch
featured: true
authors:
  - acluxace
date: Sat Oct 12 2019 17:50:55 GMT+0100 (IST)
tags:
---

Od nehody,
která se stala **29 července 2013**,
již uplynulo poměrně dost dní 
a já jsem obdržel pár e-mailů od lidí,
kteří měli tu smůlu že utrpěli stejný úraz. 
Tahle stránka je kolekce zpětné vazby kterou jsem nasbíral.

Díky všem pisatelům.
Jsem rád,
když vidím že tenhle web někdo čte.

## Jan

_16.07.2015_

> Dobrý večer,
> 
> na internetu jsem našel Váš blog ohledně prodělaného zranění ramene - kliční kosti.
> 
> Já osobně jsem tuto nepříjemnost taky podstoupil, a jsem vlastně chvíli po vyndání želez.
> Navštěvuji rehabilitace a taky s rukou už defakto plynule hýbu, nemám v ní sílu ale hýbu s ní.
> Problém mám však se samotným spojem.. to že tam mám na rameni jaksi bouli, mi nevadí, ale ta kost mi tam jaksi pruží.
> Je to normální? měl jste podobný problém?.. Jak říkám mít tam jen bouli, ale pevnou, tak mi to neva.. ale takhle mám pocit že je něco špatně.
> jsem cca 4. týden po vyndání želez a to rameno není uplně ve stejné rovině a navíc si mohu zmáčknout na bouli (kliční kost) a zatlačit ji  defakto do původní polohy.. což mě děsí.
> 
> Doufám, že Vás příliž neobtěžuji, ale jsem rád že jsme vyhledal někoho s podobným problémem a zraněním.
> 
> Děkuji

_13.10.2019_

> Co se mého zranění týče a jeho zhojení, tak mohu asi říci, že je vše již v pořádku (4 roky po úrazu). S pohybem problém nemám. 
> Snažím se cvičit, a normálně fungovat :). Jen při změnách počasí cítím v rameni bolesti, ale to je asi normální poúrazový stav. 
> 
> Problémy, které jsem dříve popsal se možná vytratily a nebo je už nevnímám. Mohu říci snad jen to, že dnes dělám vše jako dříve, 
> i když je pravda, že rozsah pohybu při různých cvicích už není takový jako dříve. 
> 
> Nejhorší byl první rok po operaci a rehabilitacích. Rameni jsem stále nevěřil, stále jsem měl pocit, že to není ono. Důvěra se mi vrátila asi až po dvou letech od finální operace. Rád bych dodal, že jsem během těch dvou let cvičil (kliky, shyby, včetně manuální práce,...), kdy po tomto jsem 
> opět začal plně ramenu věřit. Je pravda že ke sportu, JUDU, který jsem dříve před úrazem provozoval, jsem se ale už nevrátil.

> Jediné co mi dnes úraz připomíná je vlastně jizva, která je skutečně ošklivá, neboť se lehce rozšířila a dnes je velmi viditelná. Občas mě
> svědí a je to dost nepříjemné, protože to nejde  "poškrabat" :). 

## Petr
_02.12.2015_

> Dobrý den,
> 
> váš blog mě docela pobavil. Prodělal jsem nedávno na kole stejný úraz, jen mi nezkřížilo cestu policejní auto :D ....  ale jeden ze dvou psů (německý ovčák) , kteří mě předtím honili na lesní cestě. Větší strach jsem v životě neměl ...    :O
> Přiznám se to čtení mě částečně uklidňovalo, že se vše dá vyřešit a že rameno bude zase fungovat ale i částečně odradilo od operativního řešení.
> Můžu se zeptat jak je to dlouho co se to stalo a zda máte nějaké problémy?
> Je na rameni vidět jiná pozice klíční kosti nebo vám to srovnali úplně ?
> Jak dopadlo odškodnění trvalých následků ?
> 
> Možná zkusím něco někam sesmolit také, ať má někdo inspiraci i pro jiný typ léčby.

## Jano
_25.05.2017_

> zdravim, prave som docital tvoj blog
> Prave som 14 dni po operacii. Mam rovnaku diagnozu ako ty a velmi ma pobavil a hlavne vela z toho co si pisal zazivam aj ja (hlavne to kychanie 😀 ) tiez pracujem v IT 🙂 chcel som sa spytat ci ti zostali nejake trvale nasledky alebo je vsetko v pohode? 🙂 zelam pekny den 🙂

## Vláďa
_06.06.2018_

> Ahoj, tohle jsi psal Ty ? Hezky napsane, u vetsiny veci jsem jen "s usmevem" přitakával :-)
> Na podzim jsem mel podobnou operaci, tak se chci zeptat, co po 4 letech dela rameno ;-)
> Diky za info,

## Celestyn

_07.10.2019_

> Precetl jsem si Tvuj blog o ac luxaci. 
> Jsem 14 dnu po oeraci tossy iii a vsechno nachlup sedi. Jo a taky ve fn Plzen. Klasicky let na mtb prez riditka, a to jsem pak s tim ramenem jeste tyden chodil jen s mazanim a ibalginem :)
> 
> Jeste se mi ta ruka zaparila, takze jsem dostal zanet do podpazi, orteza hreje jako kamna. 
> 
> Mimochodem jak moc jsi s tou rukou hybal? Ja uz cejtim jak mi pekne ztuhlo rameno loket a zapesti. Primar ve fn zminil ze muzu vlastne ruku i kus zvednout od tela ale nesmi se zapojit ten ac kloub.
> 
> Jinak sundáni me ceka přesně po 4 tydnech, tj az 21. Mam stesti, ze uz nejsou ty horka.
>
> Ahoj a dik za inspirativni cteni

_12.10.2019_

> Jinak sundáni me ceka přesně po 4 tydnech, tj az 21. Mam stesti, ze uz nejsou ty horka.

_13.10.2019_

> Me pri propusteni ta doktorka rekla ze nesmim ruku zvednout vic nez vodorovně. Coz si prave netroufam, takze ji nechavam podel tela a natahuju si jen loket. Samozrejme trochu s ni melu i v rameni, tomu se nevyhnu. Na tej kontrole mi vlastne jen zkontroloval estli ty draty nikde necumi. Ze prej ok, tak uvidim.
> U me je to takovy pikantni tim, ze ja jsem s tim byl este tyden na docolene, kde jsem mimo jine i sekal drivi :)

## David Makovský

_20.12.2020_

> Mne se to stalo na kole, ramenem do stromu...Dopadl jsem ještě celkem dobře, žádný další zranění nebo omezení (to zápěstí k tomu muselo byt hrozný, protože máš pak celou ruku omezenou). Odvezl jsem se autem do urazovky a rentgen ukazal, že je klička o 8mm mimo, už jsem měl před pár lety sternoclavikularni luxaci s podobnou dislokaci, kterou mi neoperovali a mam tam takový pakloub, tak jsem hned říkal ať mi to rač zoperujou. Ještě se poradil s dalším doktorem (ty oprace jsou poměrně kontroverzní, někdo operuje někdo ne..) a nakonec mi řekli ať se stavím za tři dny a rozhodne se. Tak sem došel a rovnou si mě tam nechali.
>
> Za dva dny domu, s šátkem a prubanem (hrozná fixace, šátek se zařezává do krku, ruka je málo zvednutá a pruban je jak železná košile) tak jsem hned objednal ortézu. Do chvíle než přišla ortéza, tak celkem peklo, nedá se spát ani chodit ani sedět, prostě čekání na úlevu...jak jsem si to ortézou přizved a zafixoval, tak o 1000% lepší. Začlo to dobře srustat, i spát už se dalo. Po 3 týdnech kontrola, rentgen ok a řekli mi ať už rozcvičuju. Kroužit vyvěšenou rukou a že mě to nedovolí víc než 90 stupňů. Teď je to týden co cvičím a 90 stupňů dám úplně bez bolesti už. 25.12. mužů sundat ortézu, no a 8.1. du na další kontrolu. Doufám, že už se domluvíme na vytažení drátů, původně mi říkal, že je to na 6 týdnů, pak jsem četl studii o asi 50lidech a průměrně to tam měli 10-12 týdnů, tak doufám, že do února pujdou ven. Úspěšnost těch operaci byla cca 95%, ty neúspěšný byly právě ohnuti nebo migrace drátů po špatném pohybu a nebo špatně zoperovaný.
>
> Zkusim teda postupně rozsah zvětšovat, když to neboli, tak by neměl byt problém. Přes bolest raději nepujdu, zkusim si pomoct o zeď, gymnastický míč nebo tak něco. A začnu posilovat zápěstí a pak zkusím i mírnou zátěž na biceps (petlahev s vodou třeba) ať sou ty svaly trochu v pohybu, to by rehabku mohlo urychlit.

_11.01.2021_

> Tak doktor mi po kontrole zakazal cvicit a jakkoliv rameno zatěžovat. Aktivací ramenního svalu se posouvá jeden drát, za dva týdny se posunul o centimetr. Takže jen pasivně kroužit a vůbec nezatezovat. Dráty půjdou ven až v dubnu, kvuli zákazu provádět odlozitelnou péči v nemocnicích kvůli covidu. Takže se to pěkně komplikuje. 
