---
title: O AC luxaci
image: /images/o-ac-luxaci.jpg
imageMeta:
  attribution: Photo by Marcelo Leal
  attributionLink: https://unsplash.com/@marceloleal80?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText
featured: true
authors:
  - acluxace
date: Tue Feb 05 2021 17:50:55 GMT+0100 (IST)
tags:
---

## Vykloubení akromioklavikulárního kloubu

Akromioklavikulární kloub je kloub, který spojuje akromion lopatky s klíční kostí.
K jeho vykloubení dochází nejčastěji při tvrdých nárazech a pádech na rameno (např. při
hokeji) (Pilný, 2018, s. 41). Při vykloubení akromioklavikulárního kloubu dochází k bolesti při pohybu nad horizontálu. Časté jsou také noční bolesti (Kobrová, Válka, 2012, s. 83). Jaroslav Pilný ve své knize Úrazy ve sportu a jak jim předcházet rozděluje vykloubení tohoto kloubu na tři stavy podle stupně vykloubení. Při prvním stavu klíční kost zůstává na svém původním místě a objevuje se bolest v oblasti kloubu. Při druhém stavu je klíční kost posunuta ale zůstává v kontaktu s lopatkou. Dochází k poškození vazů kolem kloubu. Je také patrná drobná deformita akromioklavikulárního kloubu. Při třetím stavu dochází k úplnému vykloubení klíční kosti a ruptuře kloubních vazů. Dále je přítomna zřetelná deformita kloubu. (Pilný, 2018, s. 41-42).

Při vykloubení akromioklavikulárního kloubu je v hodné ukončit sportovní aktivitu a fixovat horní končetinu šátkovým obvazem. Pomocí RTG snímku je určen stupeň vykloubení. Další ošetření vyplývá z určeného stupně vykloubení. Při prvním stupni se horní končetina fixuje šátkovým obvazem po dobu bolesti. U druhého stupně je použit kinesiotape pro uvedení kloubu do správné polohy. Třetí stupeň vykloubení většinou vyžaduje operační
zákrok, kdy se kdy se kloub reponuje dráty nebo dlahou a sešijí se poškozené vazy (Pilný, 2018, s. 42).

-----

- PILNÝ, Jaroslav. Úrazy ve sportu a jak jim předcházet. Druhé, rozšířené a doplněné vydání. Praha: Grada Publishing, 2018. ISBN 978-80-271-0757-5.
- KOBROVÁ, Jitka a Robert VÁLKA. Terapeutické využití kinesio tapu. Praha: Grada, 2012. ISBN 978-80-247-4294-6.

-----

Zdroj: UNIVERZITA ZALACKÉHO V OLOMOUCI, FAKULTA ZDRAVOTNICKÝCH VĚD, Ústav ošetřovatelství, Jan Černý, Úrazy horních a dolních končetin ve sportu, Seminární práce, Olomouc 2021
