---
title: O blogu
image: /images/built-by.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - acluxace
date: Tue Jun 12 2018 17:50:55 GMT+0100 (IST)
tags:
---

Cílem tohoto blogu je informovat o **AC luxaci** třetího stupně. Reálný příklad vzniku. Zkušenosti s operací, pooperačním léčením, komplikacemi v životě, rehabilitací a trvalými následky.

Do článků je zařazeno hodně osobních pohledů a životních událostí. Někoho snad posty pobaví. Někomu jinému můžou přinést přehled o této životní nepříjemnosti.

V případě zájmu mě můžete kontaktovat přes e-mail: [michal.bryxi@gmail.com](mailto:michal.bryxi@gmail.com). Rád zodpovím jakékoli dotazy.
